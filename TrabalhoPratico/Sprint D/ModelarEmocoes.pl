calcularEmocoes(Utilizador, Grupo):-
	forall(
		no(X, _, _, NumeroLikes, NumeroDislikes),
		(
			DiferencaLikesDislikes is NumeroLikes - NumeroDislikes,
			% Cálculo Alegria e Angustia
			getEmocao(X, alegria, AlegriaX),
			getEmocao(X, angustia, AngustiaX),
			
			quocienteLikesDislikes(AlegriaX, AngustiaX, DiferencaLikesDislikes, NovaAlegria, NovaAngustia),
			arredondar(NovaAlegria, 2, NovaAlegriaArredondada),
			atualizarEmocao(X, alegria, NovaAlegriaArredondada),
			arredondar(NovaAngustia, 2, NovaAngustiaArredondada),
			atualizarEmocao(X, angustia, NovaAngustiaArredondada)
			%write('Nova Alegria: '), write(NovaAlegria), nl,
			%write('Nova Angustia: '), write(NovaAngustia), nl,
		)
	),
	
	
	apagaElemento(Utilizador, Grupo, GrupoSemUser),
	
	(
		esperanca(Utilizador, ListaPretendidosAtuais) -> 
		(
			% Cálculo Esperança
			findall(UserPretendido, (
				member(UserPretendido, ListaPretendidosAtuais)
			), ListaPretendidos),

			listaIncluidosExcluidos(GrupoSemUser, ListaPretendidos, ListaIncluidosEsperanca, ListaExcluidosEsperanca),

			length(ListaPretendidos, NumeroPretendidos),
			length(ListaIncluidosEsperanca, NumeroIncluidosEsperanca),
			
			getEmocao(Utilizador, esperanca, EsperancaUtilizador),
			quocienteEmocao(EsperancaUtilizador, NumeroPretendidos, NumeroIncluidosEsperanca, NovaEsperancaUtilizador),
			arredondar(NovaEsperancaUtilizador, 2, NovaEsperancaUtilizadorArredondada),
			%write('NovaEsperancaUtilizador: '), write(NovaEsperancaUtilizadorArredondada), nl,
			
			% Cálculo Dececao
			length(ListaExcluidosEsperanca, NumeroExcluidosEsperanca),
			
			getEmocao(Utilizador, dececao, DececaoUtilizador),
			quocienteEmocao(DececaoUtilizador, NumeroPretendidos, NumeroExcluidosEsperanca, NovaDececaoUtilizador),
			arredondar(NovaDececaoUtilizador, 2, NovaDececaoUtilizadorArredondada),
			%write('NovaDececaoUtilizador: '), write(NovaDececaoUtilizadorArredondada), nl,

			% Cálculo Orgulho
			getEmocao(Utilizador, orgulho, OrgulhoUtilizador),
			quocienteEmocao(OrgulhoUtilizador, NumeroPretendidos, NumeroIncluidosEsperanca, NovoOrgulhoUtilizador),
			arredondar(NovoOrgulhoUtilizador, 2, NovoOrgulhoUtilizadorArredondado),
			%write('NovoOrgulhoUtilizador: '), write(NovoOrgulhoUtilizadorArredondado), nl,
			
			% Cálculo Remorso
			getEmocao(Utilizador, remorso, RemorsoUtilizador),
			quocienteEmocao(RemorsoUtilizador, NumeroPretendidos, NumeroExcluidosEsperanca, NovoRemorsoUtilizador),
			arredondar(NovoRemorsoUtilizador, 2, NovoRemorsoUtilizadorArredondado),
			%write('NovoRemorsoUtilizador: '), write(NovoRemorsoUtilizadorArredondado), nl,
			
			atualizarVariasEmocoes(Utilizador, [(esperanca,NovaEsperancaUtilizadorArredondada),(dececao,NovaDececaoUtilizadorArredondada),(orgulho,NovoOrgulhoUtilizadorArredondado),(remorso,NovoRemorsoUtilizadorArredondado)])
		)
		;
		(
			write('Os valores da Esperanca, Dececao, Orgulho e Remorso nao foram alterados porque o Utilizador'), nl,
			write('nao pretende que nenhum utilizador em especifico seja incluido no grupo.'), nl
		)
	),

	(
		medo(Utilizador, ListaNaoPretendidosAtuais) -> 
		(
			% Cálculo Medo
			findall(UserNaoPretendido, (
				member(UserNaoPretendido, ListaNaoPretendidosAtuais)
			), ListaNaoPretendidos),

			listaIncluidosExcluidos(GrupoSemUser, ListaNaoPretendidos, ListaIncluidosMedo, ListaExcluidosMedo),

			length(ListaNaoPretendidos, NumeroNaoPretendidos),
			length(ListaIncluidosMedo, NumeroIncluidosMedo),
			
			getEmocao(Utilizador, medo, MedoUtilizador),
			quocienteEmocao(MedoUtilizador, NumeroNaoPretendidos, NumeroIncluidosMedo, NovoMedoUtilizador),
			arredondar(NovoMedoUtilizador, 2, NovoMedoUtilizadorArredondado),
			%write('NovoMedoUtilizador: '), write(NovoMedoUtilizadorArredondado), nl,
			
			% Cálculo Alivio
			length(ListaNaoPretendidos, NumeroNaoPretendidos),
			length(ListaExcluidosMedo, NumeroExcluidosMedo),
			
			getEmocao(Utilizador, alivio, AlivioUtilizador),
			quocienteEmocao(AlivioUtilizador, NumeroNaoPretendidos, NumeroExcluidosMedo, NovoAlivioUtilizador),
			arredondar(NovoAlivioUtilizador, 2, NovoAlivioUtilizadorArredondado),
			%write('NovoAlivioUtilizador: '), write(NovoAlivioUtilizadorArredondado), nl,
			
			% Cálculo Gratidao
			getEmocao(Utilizador, gratidao, GratidaoUtilizador),
			quocienteEmocao(GratidaoUtilizador, NumeroNaoPretendidos, NumeroExcluidosMedo, NovaGratidaoUtilizador),
			arredondar(NovaGratidaoUtilizador, 2, NovaGratidaoUtilizadorArredondada),
			%write('NovaGratidaoUtilizador: '), write(NovaGratidaoUtilizadorArredondada), nl,
			
			% Cálculo Raiva
			getEmocao(Utilizador, raiva, RaivaUtilizador),
			quocienteEmocao(RaivaUtilizador, NumeroNaoPretendidos, NumeroIncluidosMedo, NovaRaivaUtilizador),
			arredondar(NovaRaivaUtilizador, 2, NovaRaivaUtilizadorArredondada),
			%write('NovaRaivaUtilizador: '), write(NovaRaivaUtilizadorArredondada), nl,
			
			atualizarVariasEmocoes(Utilizador, [(medo,NovoMedoUtilizadorArredondado), (alivio,NovoAlivioUtilizadorArredondado),(gratidao,NovaGratidaoUtilizadorArredondada),(raiva,NovaRaivaUtilizadorArredondada)])
		)
		;
		(
			write('Os valores do Medo, Alivio, Gratidao e Raiva nao foram alterados porque o Utilizador'), nl,
			write('nao pretende que nenhum utilizador em especifico não esteja incluido no grupo.'), nl
		)
	).
	

getEmocao(Utilizador, Emocao, Valor):-
	emocoes(Utilizador, ListaEmocoes),
	encontrarEmocao(Emocao, ListaEmocoes, Valor).
	
	
encontrarEmocao(_, [], _):-
	fail,
	!.
	
encontrarEmocao(Emocao, [(Emocao, Valor)|_], Valor):-
	!.
	
encontrarEmocao(Emocao, [(_, _)|ListaEmocoes], Valor):-
	encontrarEmocao(Emocao, ListaEmocoes, Valor).


minimo(ValorX, ValorY, ValorFinal):-
	Diferenca is ValorX - ValorY,
	(Diferenca >= 0 -> ValorFinal is ValorY;
    (Diferenca < 0 -> ValorFinal is ValorX)).
	
	
maximo(ValorX, ValorY, ValorFinal):-
	Diferenca is ValorX - ValorY,
	(Diferenca >= 0 -> ValorFinal is ValorX;
    (Diferenca < 0 -> ValorFinal is ValorY)).
	
	
apagaElemento(_,[],[]):-
    !.

apagaElemento(X,[X|L],L):-
    !.

apagaElemento(X,[Y|L],[Y|L1]):-
    apagaElemento(X, L, L1).
	
arredondar(Valor, CasasDecimais, ValorArredondado) :- 
	Aux is Valor * 10 ^ CasasDecimais, 
	round(Aux, AuxInteiro), 
	ValorArredondado is AuxInteiro / 10 ^ CasasDecimais.

	

listaIncluidosExcluidos(_, [], [], []):-
	!.
	
listaIncluidosExcluidos(Grupo, [User|ListaPretendidos], [User|ListaIncluidos], ListaExcluidos):-
	member(User, Grupo),
	!,
	listaIncluidosExcluidos(Grupo, ListaPretendidos, ListaIncluidos, ListaExcluidos).

listaIncluidosExcluidos(Grupo, [User|ListaPretendidos], ListaIncluidos, [User|ListaExcluidos]):-
	listaIncluidosExcluidos(Grupo, ListaPretendidos, ListaIncluidos, ListaExcluidos).
	
	
quocienteEmocao(Emocao, Total, Numero, NovaEmocao):-
	NumeroMedio is Total / 2,
	(Numero > NumeroMedio -> 
		NovaEmocao is Emocao + ((1 - Emocao)*(Numero / Total));
	(Numero < NumeroMedio ->
		NovaEmocao is Emocao * (1 - (Numero / Total));
	(NovaEmocao is Emocao))).
	
quocienteLikesDislikes(Alegria, Angustia, DiferencaLikesDislikes, NovaAlegria, NovaAngustia):-
	(DiferencaLikesDislikes > 0 -> 
		(
			ValorSaturacao is 200,
			minimo(DiferencaLikesDislikes, ValorSaturacao, Diferenca),
			NovaAlegria is Alegria + ((1 - Alegria)*(Diferenca / ValorSaturacao)),
			NovaAngustia is Angustia * (1 - (Diferenca / ValorSaturacao))
		);
	(DiferencaLikesDislikes < 0 ->
		(
			ValorSaturacao is -200,
			maximo(DiferencaLikesDislikes, ValorSaturacao, Diferenca),
			NovaAlegria is Alegria * (1 - (Diferenca / ValorSaturacao)),
			NovaAngustia is Angustia + ((1 - Angustia)*(Diferenca / ValorSaturacao))
		);
	((
		NovaAlegria is Alegria,
		NovaAngustia is Angustia
	)))).
	
printEmocoesUtilizador(Utilizador):-
	emocoes(Utilizador, ListaEmocoes),
	no(Utilizador, Nome, _, _, _),
	write('Emocoes de '), write(Nome), 
	write('(ID: '), write(Utilizador), write(')'),nl,
	printEmocoesUtilizador2(ListaEmocoes).
	
printEmocoesUtilizador2([]):-
	!.

printEmocoesUtilizador2([(Emocao,Valor)|Lista]):-
	format('~w ~46t ~w~18|~n', [Emocao, Valor]),
	%write('    '),write(Emocao),write(': '),write(Valor),nl,
	printEmocoesUtilizador2(Lista).
	
atualizarEmocao(X, Emocao, Valor):-
	retract(emocoes(X, ListaEmocoes)),
	atualizarLista(ListaEmocoes, Emocao, Valor, ListaAtualizada),
	assert(emocoes(X, ListaAtualizada)).
	
atualizarLista([], _, _, []):-
	!.
	
atualizarLista([(Emocao, _)|ListaEmocoes], Emocao, Valor, [(Emocao,Valor)|ListaAtualizada]):-
	!,
	atualizarLista(ListaEmocoes, Emocao, Valor, ListaAtualizada).
	
atualizarLista([(EmocaoX,ValorX)|ListaEmocoes], Emocao, Valor, [(EmocaoX,ValorX)|ListaAtualizada]):-
	atualizarLista(ListaEmocoes, Emocao, Valor, ListaAtualizada).
	

atualizarVariasEmocoes(X, ListaNovasEmocoes):-
	retract(emocoes(X, ListaEmocoes)),
	atualizarListaEmocoes(ListaEmocoes, ListaNovasEmocoes, ListaAtualizada),
	assert(emocoes(X, ListaAtualizada)).

atualizarListaEmocoes(Lista, [], Lista):-
	!.
	
atualizarListaEmocoes(ListaEmocoes, [(Emocao, Valor)|ListaNovasEmocoes], ListaFinal):-
	atualizarLista(ListaEmocoes, Emocao, Valor, ListaAtualizada),
	atualizarListaEmocoes(ListaAtualizada, ListaNovasEmocoes, ListaFinal).
	
	