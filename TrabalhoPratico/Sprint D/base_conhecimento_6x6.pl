% Base de Conhecimento - 6 Niveis Intermédios

% no(id, nome, [tags de temas de interesse], likes, dislikes)

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto],300,200).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa],400,281).
no(12,beatriz,[natureza,musica,carros,porto,moda],196,255).
no(13,carlos,[natureza,musica,sw,futebol,coimbra],334,256).
no(14,daniel,[natureza,cinema,jogos,sw,moda],88,242).
no(15,joao,[natureza,carros,tecnologia,porto],360,120).
no(16,fernando,[natureza,teatro,tecnologia,futebol,lisboa],260,287).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra],235,325).
no(22,isabel,[natureza,musica,porto,lisboa,cinema],354,183).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa],268,192).
no(24,luisa,[natureza,cinema,jogos,moda,porto],68,253).
no(25,gaspar,[teatro,moda,jogos,lisboa],369,369).
no(26,julio,[viagens,literatura,natureza,porto],188,46).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto],66,373).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto],45,108).
no(33,andre,[natureza,carros,futebol,coimbra],399,1).
no(34,catia,[natureza,musica,cinema,lisboa,moda],260,278).
no(35,tiago,[natureza,musica,pintura,carros,coimbra],276,245).
no(36,leandro,[literatura,moda,cinema,setubal],299,44).
% Nivel 4
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto],355,25).
no(42,diogo,[natureza,futebol,sw,jogos,porto],388,205).
no(43,ernesto,[natureza,teatro,carros,porto],363,373).
no(44,isaura,[natureza,moda,tecnologia,cinema],80,343).
no(45,nurio,[viagens,pintura,literatura,aveiro],7,312).
no(46,hugo,[jogos,carros,financas,teatro],18,273).
% Nivel 5
no(51,rodolfo,[natureza,musica,sw],39,41).
no(52,joana,[natureza,fitness,viagens,lisboa],333,71).
no(53,leticia,[natureza,financas,literatura,porto],19,15).
no(54,antonia,[tecnologia,literatura,fitness,braga],129,229).
no(55,mariana,[viagens,teatro,carros,setubal],267,314).
no(56,pedro,[cinema,natureza,culinaria],142,377).
% Nivel 6
no(61,rita,[fitness,viagens,natureza],226,168).
no(62,miguel,[cinema,futebol,fotografia],232,350).
no(63,henrique,[teatro,moda,musica,financas],341,321).
no(64,clara,[tecnologia,financas,carros],181,181).
no(65,santiago,[culinaria,literatura,pintura],368,48).
no(66,salvador,[desporto,jogos,fitness],62,134).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda],29,260).
no(200,sara,[natureza,moda,musica,sw,coimbra],303,170).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,18,75,-217,-47).
ligacao(1,12,44,62,-26,-158).
ligacao(1,13,26,45,-133,145).
ligacao(1,14,12,58,-209,117).
ligacao(1,15,35,83,159,-205).
ligacao(1,16,61,32,-9,229).

% Nivel 1
ligacao(11,21,35,54,-88,216).
ligacao(11,22,22,47,-120,-310).
ligacao(11,23,43,71,167,223).
ligacao(11,24,59,98,-204,-89).
ligacao(11,25,66,84,4,-56).
ligacao(11,26,41,52,68,371).
ligacao(12,21,100,78,354,-369).
ligacao(12,22,65,72,-391,-257).
ligacao(12,23,7,56,184,-116).
ligacao(12,24,95,34,40,-36).
ligacao(12,25,93,58,-304,-118).
ligacao(12,26,33,2,355,355).
ligacao(13,21,10,4,156,7).
ligacao(13,22,99,38,163,-232).
ligacao(13,23,81,70,-216,359).
ligacao(13,24,64,60,124,-242).
ligacao(13,25,69,33,340,40).
ligacao(13,26,55,59,225,21).
ligacao(14,21,33,66,-147,-65).
ligacao(14,22,40,84,-68,399).
ligacao(14,23,82,37,-183,-291).
ligacao(14,24,94,6,165,-353).
ligacao(14,25,17,19,194,-383).
ligacao(14,26,47,84,-197,333).
ligacao(15,21,84,12,305,28).
ligacao(15,22,66,48,368,285).
ligacao(15,23,46,55,283,253).
ligacao(15,24,46,60,-379,353).
ligacao(15,25,49,64,-5,-152).
ligacao(15,26,78,44,-144,118).
ligacao(16,21,87,60,-221,22).
ligacao(16,22,71,36,319,291).
ligacao(16,23,28,24,-59,70).
ligacao(16,24,36,1,286,72).
ligacao(16,25,28,85,-177,389).
ligacao(16,26,72,57,-4,-75).
% Nivel 2
ligacao(21,31,68,57,100, 85).
ligacao(21,32,61,14,-2,116).
ligacao(21,33,25,96,-239,388).
ligacao(21,34,20,51,-277,227).
ligacao(21,35,56,16,345,280).
ligacao(21,36,100,47,-234,330).
ligacao(22,31,89,91,-231,264).
ligacao(22,32,80,90,11,375).
ligacao(22,33,5,55,-263,267).
ligacao(22,34,49,85,131,276).
ligacao(22,35,91,75,-123,41).
ligacao(22,36,15,20,-353,204).
ligacao(23,31,42,28,289,262).
ligacao(23,32,76,86,-78,110).
ligacao(23,33,32,97,-74,234).
ligacao(23,34,39,63,-95,213).
ligacao(23,35,38,38,78,225).
ligacao(23,36,51,83,60,-190).
ligacao(24,31,88,8,-104,292).
ligacao(24,32,46,15,195,-235).
ligacao(24,33,9,93,-309,-294).
ligacao(24,34,77,16,118,-268).
ligacao(24,35,91,13,-13,4).
ligacao(24,36,31,16,393,-177).
ligacao(25,31,70,72,-365,275).
ligacao(25,32,12,57,154,321).
ligacao(25,33,100,48,-331,-399).
ligacao(25,34,75,14,-97,80).
ligacao(25,35,71,50,-346,264).
ligacao(25,36,19,53,10,-126).
ligacao(26,31,31,27,-379,-72).
ligacao(26,32,22,12,98,-291).
ligacao(26,33,74,14,-13,342).
ligacao(26,34,72,97,41,-154).
ligacao(26,35,44,65,-14,1).
ligacao(26,36,23,48,86,-90).
% Nivel 3
ligacao(31,41,23,3,322,-64).
ligacao(31,42,52,79,253,105).
ligacao(31,43,74,69,-135,155).
ligacao(31,44,29,50,351,199).
ligacao(31,45,15,49,103,302).
ligacao(31,46,96,66,220,364).
ligacao(32,41,1,31,256,36).
ligacao(32,42,53,41,-218,284).
ligacao(32,43,73,13,28,-188).
ligacao(32,44,83,30,-227,51).
ligacao(32,45,73,0,-115,-140).
ligacao(32,46,26,84,-190,-333).
ligacao(33,41,21,87,-255,279).
ligacao(33,42,36,19,41,-181).
ligacao(33,43,92,67,-339,96).
ligacao(33,44,24,11,-117,65).
ligacao(33,45,52,86,391,45).
ligacao(33,46,82,40,367,44).
ligacao(34,41,48,27,-131,114).
ligacao(34,42,2,17,-63,298).
ligacao(34,43,81,89,181,60).
ligacao(34,44,45,51,-102,130).
ligacao(34,45,59,26,-271,-138).
ligacao(34,46,0,40,32,-333).
ligacao(35,41,29,87,65,-216).
ligacao(35,42,92,86,-101,-322).
ligacao(35,43,25,32,36,86).
ligacao(35,44,98,63,71,-19).
ligacao(35,45,3,14,213,-160).
ligacao(35,46,59,24,296,92).
ligacao(36,41,36,26,-78,273).
ligacao(36,42,95,80,147,381).
ligacao(36,43,84,73,82,-328).
ligacao(36,44,57,21,240,377).
ligacao(36,45,31,56,304,325).
ligacao(36,46,44,41,262,227).
% Nivel 4
ligacao(41,51,29,55,-85,-34).
ligacao(41,52,8,5,144,-191).
ligacao(41,53,46,23,-159,28).
ligacao(41,54,99,26,316,21).
ligacao(41,55,93,73,-243,-140).
ligacao(41,56,44,81,279,226).
ligacao(42,51,97,55,276,382).
ligacao(42,52,30,34,168,108).
ligacao(42,53,44,25,-138,-215).
ligacao(42,54,49,90,-257,257).
ligacao(42,55,34,95,-216,400).
ligacao(42,56,49,53,84,-100).
ligacao(43,51,5,88,354,218).
ligacao(43,52,34,10,-386,358).
ligacao(43,53,48,37,-94,-332).
ligacao(43,54,34,20,341,35).
ligacao(43,55,28,28,-214,-322).
ligacao(43,56,29,61,-329,19).
ligacao(44,51,4,2,-32,-181).
ligacao(44,52,38,89,157,-373).
ligacao(44,53,73,52,327,373).
ligacao(44,54,62,53,102,138).
ligacao(44,55,51,43,308,-223).
ligacao(44,56,33,35,-374,80).
ligacao(45,51,48,10,360,25).
ligacao(45,52,72,78,332,-394).
ligacao(45,53,64,73,-345,-69).
ligacao(45,54,65,50,369,176).
ligacao(45,55,57,89,274,-13).
ligacao(45,56,3,0,-235,-52).
ligacao(46,51,71,51,113,-80).
ligacao(46,52,32,97,366,77).
ligacao(46,53,73,53,311,130).
ligacao(46,54,35,44,25,59).
ligacao(46,55,32,10,162,-387).
ligacao(46,56,21,62,-223,368).
% Nivel 5
ligacao(51,61,17,11,-43,52).
ligacao(51,62,52,25,215,-352).
ligacao(51,63,87,60,-310,-68).
ligacao(51,64,75,80,353,-109).
ligacao(51,65,42,33,88,188).
ligacao(51,66,29,98,387,318).
ligacao(52,61,39,60,-22,204).
ligacao(52,62,10,72,-200,271).
ligacao(52,63,11,46,296,-290).
ligacao(52,64,42,29,-361,-281).
ligacao(52,65,12,49,142,-204).
ligacao(52,66,92,13,205,356).
ligacao(53,61,53,60,-134,-353).
ligacao(53,62,5,67,-268,-202).
ligacao(53,63,11,35,232,-307).
ligacao(53,64,66,61,-34,336).
ligacao(53,65,47,64,111,163).
ligacao(53,66,24,32,-1,-349).
ligacao(54,61,66,98,-18,176).
ligacao(54,62,97,35,-168,251).
ligacao(54,63,34,34,-164,180).
ligacao(54,64,34,13,-63,106).
ligacao(54,65,82,98,384,-357).
ligacao(54,66,44,86,109,235).
ligacao(55,61,64,48,380,-249).
ligacao(55,62,85,67,390,33).
ligacao(55,63,83,63,156,323).
ligacao(55,64,23,15,50,226).
ligacao(55,65,17,85,-80,-327).
ligacao(55,66,39,8,176,371).
ligacao(56,61,42,34,34,163).
ligacao(56,62,57,38,-110,385).
ligacao(56,63,54,91,12,131).
ligacao(56,64,92,32,-120,-208).
ligacao(56,65,23,48,382,160).
ligacao(56,66,78,15,-171,121).

ligacao(61,200,93,88,-123,14).
ligacao(62,200,1,13,52,-12).
ligacao(63,200,56,81,124,1).
ligacao(64,200,24,73,-51,12).
ligacao(65,200,92,100,-200,100).
ligacao(66,200,64,54,-51,90).

% Caminho Adicional
ligacao(1,199,60,65,-192,324).
ligacao(199,200,44,33,-17,91).

% Emoções
% emocoes(idUtilizador, ListaEmoções).
:- dynamic emocoes/2.
emocoes(1, [(alegria,0.6), (angustia,0.3), (esperanca,0.6), (medo,0.4), (alivio,0.1), (dececao,0.5), (orgulho,0.6), (remorso,0.4), (gratidao,0.9), (raiva,0.6)]).
emocoes(11, [(alegria,0.4), (angustia,0.4), (esperanca,0.5), (medo,0.2), (alivio,0.9), (dececao,0.9), (orgulho,0.4), (remorso,0.2), (gratidao,0.8), (raiva,0)]).
emocoes(12, [(alegria,0.5), (angustia,0.4), (esperanca,0), (medo,0.7), (alivio,0.5), (dececao,0.6), (orgulho,0.8), (remorso,0.1), (gratidao,0.6), (raiva,0.4)]).
emocoes(13, [(alegria,0.2), (angustia,0.4), (esperanca,0), (medo,0.1), (alivio,0.9), (dececao,0.2), (orgulho,0.7), (remorso,0.1), (gratidao,0.4), (raiva,0.3)]).
emocoes(14, [(alegria,0.6), (angustia,0.4), (esperanca,0.3), (medo,0.9), (alivio,0.5), (dececao,0.6), (orgulho,0.5), (remorso,0.1), (gratidao,0.7), (raiva,0.9)]).
emocoes(15, [(alegria,0.1), (angustia,0.4), (esperanca,0.9), (medo,0.7), (alivio,0.1), (dececao,0.7), (orgulho,0.3), (remorso,0.4), (gratidao,0.8), (raiva,0.4)]).
emocoes(16, [(alegria,0.7), (angustia,0.3), (esperanca,0.6), (medo,0.4), (alivio,0.7), (dececao,0.1), (orgulho,0.3), (remorso,0.1), (gratidao,0.5), (raiva,0.9)]).
emocoes(21, [(alegria,0.3), (angustia,0.1), (esperanca,0.5), (medo,0.4), (alivio,0.8), (dececao,0.9), (orgulho,0), (remorso,0.5), (gratidao,0.1), (raiva,0.6)]).
emocoes(22, [(alegria,0.1), (angustia,0.9), (esperanca,0.1), (medo,0.2), (alivio,0.5), (dececao,0.3), (orgulho,0.2), (remorso,0.8), (gratidao,0), (raiva,0.2)]).
emocoes(23, [(alegria,0.6), (angustia,0.2), (esperanca,0.4), (medo,0.5), (alivio,0.1), (dececao,0.2), (orgulho,0.1), (remorso,0), (gratidao,0.1), (raiva,0.2)]).
emocoes(24, [(alegria,0.6), (angustia,0.4), (esperanca,0.4), (medo,0), (alivio,0.1), (dececao,0.3), (orgulho,0.2), (remorso,0.7), (gratidao,0.6), (raiva,0.6)]).
emocoes(25, [(alegria,0.8), (angustia,0.8), (esperanca,0.1), (medo,0.8), (alivio,0.4), (dececao,0.3), (orgulho,0.3), (remorso,0), (gratidao,0.5), (raiva,0.5)]).
emocoes(26, [(alegria,0.3), (angustia,0.7), (esperanca,0.5), (medo,0.9), (alivio,0), (dececao,0.3), (orgulho,0.7), (remorso,0.7), (gratidao,0.3), (raiva,0.5)]).
emocoes(31, [(alegria,0.8), (angustia,0.2), (esperanca,0.3), (medo,0.2), (alivio,0.7), (dececao,0.1), (orgulho,0.2), (remorso,0.5), (gratidao,0.2), (raiva,0.2)]).
emocoes(32, [(alegria,0.7), (angustia,0.5), (esperanca,0.8), (medo,0.9), (alivio,0.4), (dececao,0.8), (orgulho,0.9), (remorso,0.9), (gratidao,0.2), (raiva,0.3)]).
emocoes(33, [(alegria,0), (angustia,0.8), (esperanca,0.8), (medo,0), (alivio,0.3), (dececao,0.6), (orgulho,0.3), (remorso,0.7), (gratidao,0.1), (raiva,0.6)]).
emocoes(34, [(alegria,0.4), (angustia,0.6), (esperanca,0.2), (medo,0.3), (alivio,0.5), (dececao,0.7), (orgulho,0.2), (remorso,0.5), (gratidao,0.4), (raiva,0.3)]).
emocoes(35, [(alegria,0.4), (angustia,0.6), (esperanca,0), (medo,0), (alivio,0), (dececao,0), (orgulho,0.3), (remorso,0), (gratidao,0), (raiva,0.8)]).
emocoes(36, [(alegria,0.4), (angustia,0.7), (esperanca,0.4), (medo,0), (alivio,0.7), (dececao,0.8), (orgulho,0.9), (remorso,0.1), (gratidao,0.3), (raiva,0)]).
emocoes(41, [(alegria,0), (angustia,0.3), (esperanca,0.1), (medo,0.3), (alivio,0), (dececao,0), (orgulho,0.2), (remorso,0.1), (gratidao,0), (raiva,0.2)]).
emocoes(42, [(alegria,0.6), (angustia,0.3), (esperanca,0), (medo,0.9), (alivio,0.5), (dececao,0.1), (orgulho,0.2), (remorso,0.3), (gratidao,0.9), (raiva,0.6)]).
emocoes(43, [(alegria,0.4), (angustia,0.4), (esperanca,0.8), (medo,0.6), (alivio,0.4), (dececao,0.5), (orgulho,0.9), (remorso,0.6), (gratidao,0.4), (raiva,0.3)]).
emocoes(44, [(alegria,0.3), (angustia,0.2), (esperanca,0.7), (medo,0.8), (alivio,0.6), (dececao,0.4), (orgulho,0.3), (remorso,0.6), (gratidao,0.4), (raiva,0.8)]).
emocoes(45, [(alegria,0), (angustia,0.1), (esperanca,0), (medo,0.1), (alivio,0.6), (dececao,0.2), (orgulho,0.1), (remorso,0.7), (gratidao,0.9), (raiva,0.9)]).
emocoes(46, [(alegria,0.2), (angustia,0), (esperanca,0.7), (medo,0.5), (alivio,0.7), (dececao,0.3), (orgulho,0.1), (remorso,0.3), (gratidao,0), (raiva,0)]).
emocoes(51, [(alegria,0.6), (angustia,0.6), (esperanca,0), (medo,0.5), (alivio,0.2), (dececao,0.4), (orgulho,0), (remorso,0.7), (gratidao,0.2), (raiva,0.5)]).
emocoes(52, [(alegria,0.6), (angustia,0.3), (esperanca,0.2), (medo,0), (alivio,0.4), (dececao,0.7), (orgulho,0.6), (remorso,0.3), (gratidao,0.1), (raiva,0.5)]).
emocoes(53, [(alegria,0), (angustia,0.8), (esperanca,0.7), (medo,0.7), (alivio,0), (dececao,0.5), (orgulho,0.4), (remorso,0.9), (gratidao,0.5), (raiva,0.8)]).
emocoes(54, [(alegria,0.4), (angustia,0.5), (esperanca,0.8), (medo,0.6), (alivio,0.3), (dececao,0.3), (orgulho,0.9), (remorso,0.1), (gratidao,0.3), (raiva,0.5)]).
emocoes(55, [(alegria,0.5), (angustia,0.6), (esperanca,0.7), (medo,0.7), (alivio,0.4), (dececao,0.3), (orgulho,0.2), (remorso,0.9), (gratidao,0.6), (raiva,0.4)]).
emocoes(56, [(alegria,0.4), (angustia,0.4), (esperanca,0.7), (medo,0), (alivio,0.1), (dececao,0.1), (orgulho,0.8), (remorso,0), (gratidao,0.7), (raiva,0.3)]).
emocoes(61, [(alegria,0.5), (angustia,0.1), (esperanca,0.8), (medo,0.9), (alivio,0.8), (dececao,0.1), (orgulho,0.6), (remorso,0.8), (gratidao,0.7), (raiva,0.5)]).
emocoes(62, [(alegria,0.9), (angustia,0.3), (esperanca,0.1), (medo,0.4), (alivio,0.6), (dececao,0.4), (orgulho,0), (remorso,0.1), (gratidao,0.7), (raiva,0.1)]).
emocoes(63, [(alegria,0.9), (angustia,0.4), (esperanca,0.2), (medo,0.1), (alivio,0.1), (dececao,0.9), (orgulho,0.7), (remorso,0.4), (gratidao,0.7), (raiva,0.8)]).
emocoes(64, [(alegria,0), (angustia,0.9), (esperanca,0.6), (medo,0.6), (alivio,0.5), (dececao,0.2), (orgulho,0.2), (remorso,0.4), (gratidao,0.6), (raiva,0.2)]).
emocoes(65, [(alegria,0.5), (angustia,0.2), (esperanca,0.8), (medo,0.8), (alivio,0.2), (dececao,0.5), (orgulho,0.2), (remorso,0.7), (gratidao,0.4), (raiva,0.7)]).
emocoes(66, [(alegria,0.1), (angustia,0.6), (esperanca,0), (medo,0.8), (alivio,0), (dececao,0.4), (orgulho,0.1), (remorso,0.1), (gratidao,0), (raiva,0.7)]).
emocoes(199, [(alegria,0.2), (angustia,0.1), (esperanca,0), (medo,0.2), (alivio,0.5), (dececao,0.4), (orgulho,0.1), (remorso,0), (gratidao,0.6), (raiva,0.3)]).
emocoes(200, [(alegria,0), (angustia,0.8), (esperanca,0), (medo,0.8), (alivio,0.1), (dececao,0.7), (orgulho,0.3), (remorso,0.1), (gratidao,0.2), (raiva,0.8)]).


esperanca(1, [11,12,13]).
medo(1, [15,16,14]).