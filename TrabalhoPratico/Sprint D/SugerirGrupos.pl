
sugerirGrupos(Utilizador, N, T, ListaTags, Resultado):-
    %Obter Tags Utilizador
    no(Utilizador,_,ListaTagsUtilizador,_,_),
    length(ListaTags, TamListaTags),

    %X � o n�mero de Tags restantes do utilizador
    X is T - TamListaTags,

    %Remover tags obrigatorias da lista de tags do utilizador
    apagaElementosListaTags(ListaTags,
    ListaTagsUtilizador,TagsCombinacoes),

    %Combina��es das restantes tags do utilizador
    bagof(R,comb(X,TagsCombinacoes,R),ListaCombinacoes),

    %Voltar a juntar as Tags obrigatorias as combinacoes
    append2(ListaCombinacoes,ListaTags, ListaTagsProcura),

    %Procurar Utilizadores com as tags de cada grupo
    procuraUtilizs(ListaTagsProcura, ListaGrupos),

    %Encontrar o maior Grupo
    maiorLista(ListaGrupos, MaiorTemp),

    %Verificar se o tamanho � maior que N
    length(MaiorTemp, Tam1),
    Tam1>=N,
    separarListas(MaiorTemp, Maior, Tags),
    write('Tags em comum: '), write(Tags), nl,
    

    copia(Maior,Resultado);
    write('N�o foi encontrado um grupo com um m�nimo de '), write(N), write(' utilizadores para as tags introduzidas').

%-----------------------------------------------------------------------
%Auxiliares

%Devolve a maior lista
maiorLista([Lista], Lista):-!.
maiorLista([H|Lista], H) :-
   length(H, Tam1),
   maiorLista(Lista, X),
   length(X, Tam2),
   Tam1 > Tam2,
   !.
maiorLista([_|Lista], X) :-
   maiorLista(Lista, X),
   !.

%Copia uma lista para outra vari�vel
copia([],[]).
copia([H|T],[H|Z]):- copia(T,Z).

%Procura grupos de utilizadores cujas tags contenham um certo conjunto
procuraUtilizs([H|ListaCombinacoes], [Grupo|ListaGrupos]):-
    procuraUtilizs(ListaCombinacoes,ListaGrupos),
    findall((Utilizador,H), (no(Utilizador,_,B,_,_), subset(H,B)), Grupo).
procuraUtilizs([],[]):-!.

%D� append de uma lista ao in�cio de todas as listas
append2([H|ListaListas], Lista, [R|Resultado]):-
    append2(ListaListas, Lista, Resultado),
    append(Lista,H,R).
append2([],_,[]):-!.

%Combina��es dos elementos de uma lista N a N
comb(0,_,[]).
comb(N,[X|T],[X|Comb]):-N>0,N1 is N-1,comb(N1,T,Comb).
comb(N,[_|T],Comb):-N>0,comb(N,T,Comb).

%Apaga os elementos de uma lista noutra, gerando uma nova
apagaElementosListaTags([H|ListaTags], ListaTagsUtilizador, Resultado):-
   subtract(ListaTagsUtilizador,[H],Resultado1),
   apagaElementosListaTags(ListaTags,Resultado1,Resultado).
apagaElementosListaTags([],Resultado, Resultado):-!.

separarListas([(U,T)], [], T).

separarListas([(U,T)|Lista], [U|Resultado], Tags):-
    separarListas(Lista, Resultado, Tags).
