% Sprint D

% --------------------------------------------------------------
% Contador de Soluções
:- dynamic contadorSolucoes/1.

% Facto Dinâmico - melhorSolucaoMaisForte(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMaisForteNLigacoes/2.

% Facto Dinâmico - melhorSolucaoMaisForte(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMultiCriterioNLigacoes/2.

% Sprint D - Funcao MultiCriterio
funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, Resultado):-
    FatorLigacao is 0.5 * ForcaLigacao,
    FatorRel is (0.5 * (50 + 0.25 * ForcaRelacao)),

    (FatorRel > 50 -> FatorRelacao is 50;
    (FatorRel < 0 -> FatorRelacao is 0;
    (FatorRelacao is FatorRel))),

    Resultado is FatorLigacao + FatorRelacao.

% --------------------------------------------------------------
% Caminho Mais Forte N Ligacoes - Sentido da Travessia (Unidirecional)
dfsCaminhoMaisForteNLigacoes(Origem, Destino, NumeroLigacoes, Caminho, ForcaCaminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisForteNLigacoes(Origem, NumeroLigacoes, Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisForteNLigacoes(Caminho,ForcaCaminho)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisForteNLigacoes(Origem, NumeroLigacoes, Destino):-
		% Define o melhor caminho com força igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMaisForteNLigacoes(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroLigacoes, Caminho),
		atualizaMelhorCaminhoMaisForteNLigacoes(Caminho),

		fail.

atualizaMelhorCaminhoMaisForteNLigacoes(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMaisForteNLigacoes(_, ForcaAtual),
		calculaForcaLigacaoUnidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMaisForteNLigacoes(_,_)),
		asserta(melhorSolucaoMaisForteNLigacoes(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, não será possível calcular mais forças
calculaForcaLigacaoUnidirecional([_], 0):-
	!.

calculaForcaLigacaoUnidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaForcaLigacaoUnidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, F1, _, _, _),
	Resultado is Forca + F1.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Caminho Multi Criterio N Ligacoes - Sentido da Travessia (Unidirecional)
dfsCaminhoMultiCriterioNLigacoes(Origem, Destino, NumeroLigacoes, Caminho, FatorMultiCriterio):-
		get_time(TempoInicio),

		(melhorCaminhoMultiCriterioNLigacoes(Origem, NumeroLigacoes, Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMultiCriterioNLigacoes(Caminho, FatorMultiCriterio)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMultiCriterioNLigacoes(Origem, NumeroLigacoes, Destino):-
		% Define o melhor caminho com força igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMultiCriterioNLigacoes(_, -9999)),
		asserta(contadorSolucoes(0)),

		dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroLigacoes, Caminho),
		atualizaMelhorCaminhoMultiCriterioNLigacoes(Caminho),

		fail.

atualizaMelhorCaminhoMultiCriterioNLigacoes(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMultiCriterioNLigacoes(_, MultiCriterioAtual),
		calculaMultiCriterioUnidirecional(Caminho, MultiCriterio),
		MultiCriterio > MultiCriterioAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMultiCriterioNLigacoes(_,_)),
		asserta(melhorSolucaoMultiCriterioNLigacoes(Caminho, MultiCriterio)).

% Quando apenas existir um elemento no caminho, não será possível calcular mais forças
calculaMultiCriterioUnidirecional([_], 0):-
	!.

calculaMultiCriterioUnidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaMultiCriterioUnidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	Resultado is Forca + FatorMultiCriterio.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Best First Força de Ligação
bestFirstForcaLigacao(Origem, Destino, MaxLigacoes, Caminho, Custo):-
	get_time(TempoInicio),

	bestFirstForcaLigacao2(Destino, [[Origem]], MaxLigacoes, Caminho,Custo),

	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
%	format('~10f', TempoExecucao),
	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

bestFirstForcaLigacao2(Destino, [[Destino|T]|_], MaxLigacoes, Caminho, Custo):-
	reverse([Destino|T], Caminho),
	length(Caminho, Temp),
	NumeroLigacoes is Temp - 1,
	NumeroLigacoes =< MaxLigacoes,
	calculaCustoForcaLigacao(Caminho,Custo).

bestFirstForcaLigacao2(Destino, [[Destino|_]|ListaCaminhos], MaxLigacoes, Caminho, Custo):-
	!,
	bestFirstForcaLigacao2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo).

bestFirstForcaLigacao2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo):-
	primeiroElemento(CaminhoAtual, ListaCaminhos, ListaCaminhosAtualizada),
	CaminhoAtual = [Atual|_],
	(
	    (
		    Atual == Destino,
			!,
			bestFirstForcaLigacao2(Destino, [CaminhoAtual|ListaCaminhosAtualizada], MaxLigacoes, Caminho, Custo)
		)
	    ;
	    (
	        findall((ForcaLigacao,[X|CaminhoAtual]), (
			    ligacao(Atual, X, ForcaLigacao, _, _, _),
	            \+member(X, CaminhoAtual),
				validaEmocoes(X, Destino)
			), Novos),
	        Novos \== [],
			!,
	        sort(0, @>=, Novos, NovosOrdenados),
	        retiraCustos(NovosOrdenados, NovosOrdenadosNormalizados),
	        append(NovosOrdenadosNormalizados, ListaCaminhosAtualizada, ListaCaminhosCompleta),
	        bestFirstForcaLigacao2(Destino, ListaCaminhosCompleta, MaxLigacoes, Caminho, Custo)
	    )
	).

% Devolve o Primeiro elemento de uma Lista no primeiro argumento e devolve a lista sem esse elemento no terceiro argumento
primeiroElemento(PrimeiroElemento, [PrimeiroElemento|Lista], Lista).
primeiroElemento(PrimeiroElemento, [_|Lista], ListaFinal):-
    primeiroElemento(PrimeiroElemento, Lista, ListaFinal).

% Retira o Custo presente numa lista de tuplos onde [(Custo, Caminho)]
retiraCustos([],[]).
retiraCustos([(_,LA)|L],[LA|L1]):-
    retiraCustos(L,L1).

calculaCustoForcaLigacao([Atual,X], Custo):-
    !,
	ligacao(Atual, X, Custo, _, _, _).

calculaCustoForcaLigacao([Atual, X|L], Resultado):-
    calculaCustoForcaLigacao([X|L], Resultado1),
	ligacao(Atual, X, ForcaLigacao, _, _, _),
	Resultado is Resultado1 + ForcaLigacao.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Best First Multi Criterio
bestFirstMultiCriterio(Origem, Destino, MaxLigacoes, Caminho, Custo):-
	get_time(TempoInicio),

	bestFirstMultiCriterio2(Destino, [[Origem]], MaxLigacoes, Caminho, Custo),

	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').


bestFirstMultiCriterio2(Destino, [[Destino|T]|_], MaxLigacoes, Caminho, Custo):-
	reverse([Destino|T], Caminho),
	length(Caminho, Temp),
	NumeroLigacoes is Temp - 1,
    NumeroLigacoes =< MaxLigacoes,
	calculaCustoMultiCriterio(Caminho, Custo).

bestFirstMultiCriterio2(Destino, [[Destino|_]|ListaCaminhos], MaxLigacoes, Caminho, Custo):-
	!,
	bestFirstMultiCriterio2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo).


bestFirstMultiCriterio2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo):-
	primeiroElemento(CaminhoAtual, ListaCaminhos, ListaCaminhosAtualizada),
	CaminhoAtual = [Atual|_],
	(
	    (
		    Atual == Destino,
			!,
			bestFirstMultiCriterio2(Destino, [CaminhoAtual|ListaCaminhosAtualizada], MaxLigacoes, Caminho, Custo)
		)
	    ;
	    (
	        findall((FatorMultiCriterio, [X|CaminhoAtual]), (
			    ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	            \+member(X, CaminhoAtual),
				validaEmocoes(X, Destino),
				funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio)
			), Novos),
	        Novos \==[],
			!,
	        sort(0, @>=, Novos, NovosOrdenados),
	        retiraCustos(NovosOrdenados, NovosOrdenadosNormalizados),
	        append(NovosOrdenadosNormalizados, ListaCaminhosAtualizada, ListaCaminhosCompleta),
	        bestFirstMultiCriterio2(Destino, ListaCaminhosCompleta, MaxLigacoes, Caminho, Custo)
	    )
	).

calculaCustoMultiCriterio([Atual,X], Custo):-
    !,
	ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, Custo).

calculaCustoMultiCriterio([Atual, X|L], Resultado):-
    calculaCustoMultiCriterio([X|L], Resultado1),
	ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	Resultado is Resultado1 + FatorMultiCriterio.

% --------------------------------------------------------------


% --------------------------------------------------------------
% A* - Forca de Ligacao
aStarForcaLigacao(Origem, Destino, NumeroMaximoLigacoes, Caminho, Custo):-
    get_time(TempoInicio),

    aStarForcaLigacao2(Destino, [(_, 0, [Origem], [])], NumeroMaximoLigacoes, Caminho, Custo),
	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	%format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

% A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
aStarForcaLigacao2(Destino,[(_, Custo, [Destino|Tail], ListaLigacoes)|_], NumeroMaximoLigacoes, Caminho, Custo):-
	length(ListaLigacoes, NumeroLigacoes),
	NumeroLigacoes =< NumeroMaximoLigacoes,
    reverse([Destino|Tail], Caminho).

aStarForcaLigacao2(Destino,[(_, CustoAtual, CaminhoPercorridoAtual, ListaLigacoes)|Outros], NumeroMaximoLigacoes, Caminho, Custo):-
    CaminhoPercorridoAtual = [Atual|_],
    findall((CustoEstimadoX, CustoAteX, [X|CaminhoPercorridoAtual], [CustoX|ListaLigacoes]), (
        Destino \== Atual,									% Verifica se o Nó Atual não é igual ao Destino
		ligacao(Atual, X, CustoX, _, _, _),					% Obtem o custo do Nó Atual até X
		validaEmocoes(X, Destino),
        \+ member(X, CaminhoPercorridoAtual),				% Verifica se X não pertence ao Caminho Atual
        estimativaForcaLigacao(X, ListaLigacoes, NumeroMaximoLigacoes, EstimativaX),	% EstimativaX - Calcula a estimativa desde X até ao Destino
		CustoAteX is CustoX + CustoAtual,					% Custo até X - igual ao CustoX + Custo Atual do caminho
        CustoEstimadoX is CustoAteX + EstimativaX			% Custo Estimado é igual a Custo Atual do caminho + Estimativa X
	), Novos),
    append(Outros, Novos, Todos),
    sort(0, @>=, Todos, TodosOrdenados),
    aStarForcaLigacao2(Destino, TodosOrdenados, NumeroMaximoLigacoes, Caminho, Custo).

estimativaForcaLigacao(No, ListaLigacoes, NumeroMaximoLigacoes, Estimativa):-
    length(ListaLigacoes, NumeroLigacoesAtual),
	Nivel is NumeroMaximoLigacoes - NumeroLigacoesAtual,
	forcasLigacaoUtilizador(No, Nivel, ListaForcas),
    sort(0, @>=, ListaForcas, ListaForcasOrdenada),
    removeForcasUtilizadas(ListaLigacoes, ListaForcasOrdenada, ListaForcasAtualizada),
    primeirosNElementos(ListaForcasAtualizada, Nivel, ListaForcasFinal),
	soma(ListaForcasFinal, Estimativa).

% Retorna as forças de ligação do Utilizador para determinado Nivel
forcasLigacaoUtilizador(Utilizador, Nivel, ListaForcas):-
	tamanhoRedeUtilizador(Utilizador, Nivel, ListaUtilizadoresRede, _),
	forcaLigacaoLista(ListaUtilizadoresRede, [], ListaForcas).

forcaLigacaoLista([], ListaFinal, ListaFinal):-
	!.

forcaLigacaoLista([Utilizador|ListaUtilizadoresRede], ListaLigacoesAtual, ListaFinal):-
    findall(ForcaLigacao, (
	    member(X, ListaUtilizadoresRede),
	    X \== Utilizador,
		forcaLigacao(Utilizador, X, ForcaLigacao)
    ),
	ListaLigacoes),

	append(ListaLigacoes, ListaLigacoesAtual, ListaLigacoesCompleta),

	forcaLigacaoLista(ListaUtilizadoresRede, ListaLigacoesCompleta, ListaFinal).


% Força de ligação entre X e Y
forcaLigacao(X,Y, ForcaLigacao):-
    ligacao(X,Y,ForcaLigacao,_,_,_),
	!.

forcaLigacao(X,Y,ForcaLigacao):-
    ligacao(Y,X,_,ForcaLigacao,_,_),
	!.

forcaLigacao(_,_, _):-
	fail,
	!.

% --------------------------------------------------------------


% --------------------------------------------------------------
% A* - Multi Criterio
aStarMultiCriterio(Origem, Destino, NumeroMaximoLigacoes, Caminho, Custo):-
	get_time(TempoInicio),
	
    aStarMultiCriterio2(Destino, [(_, 0, [Origem], [])], NumeroMaximoLigacoes, Caminho, Custo),get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	%format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

% A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
aStarMultiCriterio2(Destino,[(_, Custo, [Destino|Tail], ListaLigacoes)|_], NumeroMaximoLigacoes, Caminho, Custo):-
	length(ListaLigacoes, NumeroLigacoes),
	NumeroLigacoes =< NumeroMaximoLigacoes,
    reverse([Destino|Tail], Caminho).

aStarMultiCriterio2(Destino,[(_, CustoAtual, CaminhoPercorridoAtual, ListaLigacoes)|Outros], NumeroMaximoLigacoes, Caminho, Custo):-
    CaminhoPercorridoAtual = [Atual|_],
    findall((CustoEstimadoX, CustoAteX, [X|CaminhoPercorridoAtual], [CustoX|ListaLigacoes]), (
        Destino \== Atual,										% Verifica se o Nó Atual não é igual ao Destino
		ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),	% Obtem o custo do Nó Atual até X
		validaEmocoes(X, Destino),
		\+ member(X, CaminhoPercorridoAtual),					% Verifica se X não pertence ao Caminho Atual
        funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, CustoX),
        estimativaMultiCriterio(X, ListaLigacoes, NumeroMaximoLigacoes, EstimativaX),	% EstimativaX - Calcula a estimativa desde X até ao Destino
		CustoAteX is CustoX + CustoAtual,						% Custo até X - igual ao CustoX + Custo Atual do caminho
        CustoEstimadoX is CustoAteX + EstimativaX				% Custo Estimado é igual a Custo Atual do caminho + Estimativa X
	), Novos),
    append(Outros, Novos, Todos),
    sort(0, @>=, Todos, TodosOrdenados),
    aStarMultiCriterio2(Destino, TodosOrdenados, NumeroMaximoLigacoes, Caminho, Custo).

estimativaMultiCriterio(No, ListaLigacoes, NumeroMaximoLigacoes, Estimativa):-
    length(ListaLigacoes, NumeroLigacoesAtual),
	Nivel is NumeroMaximoLigacoes - NumeroLigacoesAtual,
	forcasMultiCriterioUtilizador(No, Nivel, ListaForcas),
    sort(0, @>=, ListaForcas, ListaForcasOrdenada),
    removeForcasUtilizadas(ListaLigacoes, ListaForcasOrdenada, ListaForcasAtualizada),
    primeirosNElementos(ListaForcasAtualizada, Nivel, ListaForcasFinal),
	soma(ListaForcasFinal, Estimativa).

% Retorna as forças de ligação do Utilizador para determinado Nivel
forcasMultiCriterioUtilizador(Utilizador, Nivel, ListaForcas):-
	tamanhoRedeUtilizador(Utilizador, Nivel, ListaUtilizadoresRede, _),
	forcaMultiCriterioLista(ListaUtilizadoresRede, [], ListaForcas).

forcaMultiCriterioLista([], ListaFinal, ListaFinal):-
	!.

forcaMultiCriterioLista([Utilizador|ListaUtilizadoresRede], ListaLigacoesAtual, ListaFinal):-
    findall(FatorMultiCriterio, (
	    member(X, ListaUtilizadoresRede),
	    X \== Utilizador,
		forcaMultiCriterio(Utilizador, X, FatorMultiCriterio)
    ),
	ListaLigacoes),

	append(ListaLigacoes, ListaLigacoesAtual, ListaLigacoesCompleta),

	forcaMultiCriterioLista(ListaUtilizadoresRede, ListaLigacoesCompleta, ListaFinal).


% Força Multi Criterio entre X e Y
forcaMultiCriterio(X, Y, FatorMultiCriterio):-
    ligacao(X, Y, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	!.

forcaMultiCriterio(X, Y, FatorMultiCriterio):-
    ligacao(Y, X, _, ForcaLigacao, _, ForcaRelacao),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	!.

forcaMultiCriterio(_,_, _):-
	fail,
	!.

% --------------------------------------------------------------


% --------------------------------------------------------------
% AUXILIARES

% Verifica se existe ligação entre dois Utilizadores - Unidirecional
connectUnidirecional(X,Y):-
    ligacao(X,Y,_,_,_,_).


% Apaga 1 Ocorrencia de X Em L e devolve L1 no 3º argumento
apaga1(_,[],[]):-
    !.

apaga1(X,[X|L],L):-
    !.

apaga1(X,[Y|L],[Y|L1]):-
    apaga1(X, L, L1).


% Soma de elementos de uma Lista
soma([], 0):-
    !.

soma([X|Lista], Total):-
    soma(Lista, Total1),
    Total is Total1 + X.


% Remove Elementos da Lista do 1º argumento que se encontram na lista do 2º argumento
removeForcasUtilizadas([], ListaForcas, ListaForcas):-
	!.

removeForcasUtilizadas([Ligacao|ListaLigacoes], ListaForcas, Final):-
	apaga1(Ligacao, ListaForcas, NovaLista),
	removeForcasUtilizadas(ListaLigacoes, NovaLista, Final).

primeirosNElementos([], _, []):-
    !.

primeirosNElementos(_, 0, []):-
    !.

primeirosNElementos([Cabeca|ListaForcasOrdenada], NumeroMaximoLigacoes, [Cabeca|ListaFinal]):-
    ContadorLigacoes is NumeroMaximoLigacoes - 1,
    primeirosNElementos(ListaForcasOrdenada, ContadorLigacoes, ListaFinal).


% --------------------------------------------------------------
% Tamanho da Rede de um Utilizador
tamanhoRedeUtilizador(Utilizador, Nivel, ListaFinal, Tamanho):-
    tamanhoRede2(Nivel,[Utilizador],[],[Utilizador],ListaFinal),
	length(ListaFinal, TamanhoListaFinal),
    % Retirar o primeiro Utilizador
    Tamanho is TamanhoListaFinal - 1,
	!.

% Quando as duas listas de queues se encontram vazias, já não existem mais
% nós a percorrer na rede
tamanhoRede2(_,[], [], ListaFinal, ListaFinal):-
    !.

% Quando a queue se encontra vazia, decrementa-se o nível e substitui-se
% a queue por todos os nós visitados no nível anterior
tamanhoRede2(Nivel,[], ProxQueue, ListaFinal, ListaResultado):-
    NivelAtual is Nivel - 1,
    tamanhoRede2(NivelAtual, ProxQueue, [], ListaFinal, ListaResultado).

% Quando o nível chega a 0, o tamanho será a length da ListaFinal - 1
% para retirar o utilizador inicial
tamanhoRede2(0,_,_,ListaFinal,ListaFinal):-
    !.

tamanhoRede2(Nivel, [Utilizador|QueueUtilizadores], ProximaQueue, ListaFinal, ListaResultado):-
    % Preencher ListaUserNivel com todos os X
    % X será utilizadores conectados ao primeiro Utilizador na queue e que não
    % se encontrem na Lista Final
    findall(X,(
                connectUnidirecional(Utilizador, X),
                % \+member(X, [Utilizador|QueueUtilizadores]),
                \+member(X, ListaFinal)
            ),
            ListaUserNivel),

    % Adicionar a lista de nós visitados à Lista Final
    append(ListaUserNivel, ListaFinal, ListaCompleta),

    % write(ListaCompleta),nl,

    % Criar lista de nós visitados sem nós que já se encontrem em queue
    findall(Y,(
                member(Y, ListaUserNivel),
                \+member(Y, ProximaQueue)
            ), UserNivelSemRepetidos),

    % Adicionar os nós visitados que ainda não estão na queue à proxima queue
    append(UserNivelSemRepetidos, ProximaQueue, NovaQueue),

    % Chamada Recursiva
    tamanhoRede2(Nivel, QueueUtilizadores, NovaQueue, ListaCompleta, ListaResultado).

% --------------------------------------------------------------


% --------------------------------------------------------------
% Primeiro em Profundidade Caminho Mais Forte N Ligacoes - Unidirecional
dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroMaxLigacoes, Caminho):-
    dfsCaminhoMaisForteNLigacoes2(Origem, Destino, [Origem], NumeroMaxLigacoes, Caminho).

% Condição Final: User Atual = Destino
dfsCaminhoMaisForteNLigacoes2(Destino, Destino, ListaAux, _, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsCaminhoMaisForteNLigacoes2(UserAtual, Destino, ListaAux, NumeroMaxLigacoes, Caminho):-
	length(ListaAux, LengthCaminho),
	LengthCaminho =< NumeroMaxLigacoes,

    % Testar Ligação entre NoAtual e qualquer X
    connectUnidirecional(UserAtual, X),
	
	% Testar Emoções a menos que seja para o nó final
	validaEmocoes(X, Destino),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsCaminhoMaisForteNLigacoes2(X, Destino, [X|ListaAux], NumeroMaxLigacoes, Caminho).


validaEmocoes(Utilizador, Utilizador):-
	!.
	
validaEmocoes(Utilizador, _):-
	validaEmocoes(Utilizador),
	!.

validaEmocoes(Utilizador):-
	% Angustia
	getEmocao(Utilizador, angustia, ValorAngustia),
	ValorAngustia =< 0.5,
	
	% Medo
	getEmocao(Utilizador, medo, ValorMedo),
	ValorMedo =< 0.5,
	
	% Dececao
	getEmocao(Utilizador, dececao, ValorDececao),
	ValorDececao =< 0.5,
	
	% Remorso
	getEmocao(Utilizador, remorso, ValorRemorso),
	ValorRemorso =< 0.5,
	
	% Raiva
	getEmocao(Utilizador, raiva, ValorRaiva),
	ValorRaiva =< 0.5,
	
	!.
	

getEmocao(Utilizador, Emocao, Valor):-
	emocoes(Utilizador, ListaEmocoes),
	encontrarEmocao(Emocao, ListaEmocoes, Valor).
	
	
encontrarEmocao(_, [], _):-
	fail,
	!.
	
encontrarEmocao(Emocao, [(Emocao, Valor)|_], Valor):-
	!.
	
encontrarEmocao(Emocao, [(_, _)|ListaEmocoes], Valor):-
	encontrarEmocao(Emocao, ListaEmocoes, Valor).


minimo(ValorX, ValorY, ValorFinal):-
	Diferenca is ValorX - ValorY,
	(Diferenca >= 0 -> ValorFinal is ValorY;
    (Diferenca < 0 -> ValorFinal is ValorX)).
	
utilizadoresValidos(ListaUtilizadores):-
	findall(X, (
		no(X, _, _, _, _),
		validaEmocoes(X, 0)
	), ListaUtilizadores).