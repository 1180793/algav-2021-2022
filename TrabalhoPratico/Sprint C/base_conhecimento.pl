% Base de Conhecimento

% no(id, nome, [tags de temas de interesse])

no(1,ana,[natureza,pintura,musica,sw,porto]).
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
no(13,carlos,[natureza,musica,sw,futebol,coimbra]).
no(14,daniel,[natureza,cinema,jogos,sw,moda]).
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa]).
no(24,luisa,[natureza,cinema,jogos,moda,porto]).
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
no(33,andre,[natureza,carros,futebol,coimbra]).
no(34,catia,[natureza,musica,cinema,lisboa,moda]).
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto]).
no(42,diogo,[natureza,futebol,sw,jogos,porto]).
no(43,ernesto,[natureza,teatro,carros,porto]).
no(44,isaura,[natureza,moda,tecnologia,cinema]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

no(51,rodolfo,[natureza,musica,sw]).
no(61,rita,[moda,tecnologia,cinema]).


% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1, forcaRelacaoEntre1e2, forcaRelacaoEntre2e1)

ligacao(1,11,18,75,-217,-47).
ligacao(1,12,44,62,-26,-158).
ligacao(1,13,26,45,-133,145).
ligacao(1,14,12,58,-209,117).

ligacao(11,21,35,54,-88,216).
ligacao(11,22,22,47,-120,-310).
ligacao(11,23,43,71,167,223).
ligacao(11,24,59,98,-204,-89).
ligacao(12,21,100,78,354,-369).
ligacao(12,22,65,72,-391,-257).
ligacao(12,23,7,56,184,-116).
ligacao(12,24,95,34,40,-36).
ligacao(13,21,10,4,156,7).
ligacao(13,22,99,38,163,-232).
ligacao(13,23,81,70,-216,359).
ligacao(13,24,64,60,124,-242).
ligacao(14,21,33,66,-147,-65).
ligacao(14,22,40,84,-68,399).
ligacao(14,23,82,37,-183,-291).
ligacao(14,24,94,6,165,-353).
ligacao(21,31,68,57,100, 85).
ligacao(21,32,61,14,-2,116).
ligacao(21,33,25,96,-239,388).
ligacao(21,34,20,51,-277,227).
ligacao(22,31,89,91,-231,264).
ligacao(22,32,80,90,11,375).
ligacao(22,33,5,55,-263,267).
ligacao(22,34,49,85,131,276).
ligacao(23,31,42,28,289,262).
ligacao(23,32,76,86,-78,110).
ligacao(23,33,32,97,-74,234).
ligacao(23,34,39,63,-95,213).
ligacao(24,31,88,8,-104,292).
ligacao(24,32,46,15,195,-235).
ligacao(24,33,9,93,-309,-294).
ligacao(24,34,77,16,118,-268).
ligacao(31,41,23,3,322,-64).
ligacao(31,42,52,79,253,105).
ligacao(31,43,74,69,-135,155).
ligacao(31,44,29,50,351,199).
ligacao(32,41,1,31,256,36).
ligacao(32,42,53,41,-218,284).
ligacao(32,43,73,13,28,-188).
ligacao(32,44,83,30,-227,51).
ligacao(33,41,21,87,-255,279).
ligacao(33,42,36,19,41,-181).
ligacao(33,43,92,67,-339,96).
ligacao(33,44,24,11,-117,65).
ligacao(34,41,48,27,-131,114).
ligacao(34,42,2,17,-63,298).
ligacao(34,43,81,89,181,60).
ligacao(34,44,45,51,-102,130).
ligacao(41,200,47,54,340,-389).
ligacao(42,200,92,50,-345,27).
ligacao(43,200,55,9,87,-212).
ligacao(44,200,98,91,304,45).

ligacao(1,51,6,21,183,-252).
ligacao(51,61,78,13,-396,-272).
ligacao(61,200,76,49,55,19).

