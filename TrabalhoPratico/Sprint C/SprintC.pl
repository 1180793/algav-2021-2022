% Sprint C

% --------------------------------------------------------------
% Contador de Solu��es
:- dynamic contadorSolucoes/1.

% Facto Din�mico - melhorSolucaoMaisForte(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMaisForteNLigacoes/2.

% Facto Din�mico - melhorSolucaoMaisForte(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMultiCriterioNLigacoes/2.

% Sprint C - Funcao MultiCriterio
funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, Resultado):-
    FatorLigacao is 0.5 * ForcaLigacao,
    FatorRel is (0.5 * (50 + 0.25 * ForcaRelacao)),

    (FatorRel > 50 -> FatorRelacao is 50;
    (FatorRel < 0 -> FatorRelacao is 0;
    (FatorRelacao is FatorRel))),

    Resultado is FatorLigacao + FatorRelacao.

% --------------------------------------------------------------
% Caminho Mais Forte N Ligacoes - Sentido da Travessia (Unidirecional)
dfsCaminhoMaisForteNLigacoes(Origem, Destino, NumeroLigacoes, Caminho, ForcaCaminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisForteNLigacoes(Origem, NumeroLigacoes, Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisForteNLigacoes(Caminho,ForcaCaminho)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisForteNLigacoes(Origem, NumeroLigacoes, Destino):-
		% Define o melhor caminho com for�a igual a 0 para garantir que ser�
		% substituido na primeira itera��o
		asserta(melhorSolucaoMaisForteNLigacoes(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroLigacoes, Caminho),
		atualizaMelhorCaminhoMaisForteNLigacoes(Caminho),

		fail.

atualizaMelhorCaminhoMaisForteNLigacoes(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a For�a do Caminho atualmente registado como sendo o melhor
		% com a For�a do Caminho Recebido
		melhorSolucaoMaisForteNLigacoes(_, ForcaAtual),
		calculaForcaLigacaoUnidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), � realizada a substitui��o
		retract(melhorSolucaoMaisForteNLigacoes(_,_)),
		asserta(melhorSolucaoMaisForteNLigacoes(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, n�o ser� poss�vel calcular mais for�as
calculaForcaLigacaoUnidirecional([_], 0):-
	!.

calculaForcaLigacaoUnidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaForcaLigacaoUnidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, F1, _, _, _),
	Resultado is Forca + F1.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Caminho Multi Criterio N Ligacoes - Sentido da Travessia (Unidirecional)
dfsCaminhoMultiCriterioNLigacoes(Origem, Destino, NumeroLigacoes, Caminho, FatorMultiCriterio):-
		get_time(TempoInicio),

		(melhorCaminhoMultiCriterioNLigacoes(Origem, NumeroLigacoes, Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMultiCriterioNLigacoes(Caminho, FatorMultiCriterio)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMultiCriterioNLigacoes(Origem, NumeroLigacoes, Destino):-
		% Define o melhor caminho com for�a igual a 0 para garantir que ser�
		% substituido na primeira itera��o
		asserta(melhorSolucaoMultiCriterioNLigacoes(_, -9999)),
		asserta(contadorSolucoes(0)),

		dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroLigacoes, Caminho),
		atualizaMelhorCaminhoMultiCriterioNLigacoes(Caminho),

		fail.

atualizaMelhorCaminhoMultiCriterioNLigacoes(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a For�a do Caminho atualmente registado como sendo o melhor
		% com a For�a do Caminho Recebido
		melhorSolucaoMultiCriterioNLigacoes(_, MultiCriterioAtual),
		calculaMultiCriterioUnidirecional(Caminho, MultiCriterio),
		MultiCriterio > MultiCriterioAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), � realizada a substitui��o
		retract(melhorSolucaoMultiCriterioNLigacoes(_,_)),
		asserta(melhorSolucaoMultiCriterioNLigacoes(Caminho, MultiCriterio)).

% Quando apenas existir um elemento no caminho, n�o ser� poss�vel calcular mais for�as
calculaMultiCriterioUnidirecional([_], 0):-
	!.

calculaMultiCriterioUnidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaMultiCriterioUnidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	Resultado is Forca + FatorMultiCriterio.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Best First For�a de Liga��o
bestFirstForcaLigacao(Origem, Destino, MaxLigacoes, Caminho, Custo):-
	get_time(TempoInicio),

	bestFirstForcaLigacao2(Destino, [[Origem]], MaxLigacoes, Caminho,Custo),

	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
%	format('~10f', TempoExecucao),
	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

bestFirstForcaLigacao2(Destino, [[Destino|T]|_], MaxLigacoes, Caminho, Custo):-
	reverse([Destino|T], Caminho),
	length(Caminho, Temp),
	NumeroLigacoes is Temp - 1,
	NumeroLigacoes =< MaxLigacoes,
	calculaCustoForcaLigacao(Caminho,Custo).

bestFirstForcaLigacao2(Destino, [[Destino|_]|ListaCaminhos], MaxLigacoes, Caminho, Custo):-
	!,
	bestFirstForcaLigacao2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo).

bestFirstForcaLigacao2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo):-
	primeiroElemento(CaminhoAtual, ListaCaminhos, ListaCaminhosAtualizada),
	CaminhoAtual = [Atual|_],
	(
	    (
		    Atual == Destino,
			!,
			bestFirstForcaLigacao2(Destino, [CaminhoAtual|ListaCaminhosAtualizada], MaxLigacoes, Caminho, Custo)
		)
	    ;
	    (
	        findall((ForcaLigacao,[X|CaminhoAtual]), (
			    ligacao(Atual, X, ForcaLigacao, _, _, _),
	            \+member(X, CaminhoAtual)
			), Novos),
	        Novos \== [],
			!,
	        sort(0, @>=, Novos, NovosOrdenados),
	        retiraCustos(NovosOrdenados, NovosOrdenadosNormalizados),
	        append(NovosOrdenadosNormalizados, ListaCaminhosAtualizada, ListaCaminhosCompleta),
	        bestFirstForcaLigacao2(Destino, ListaCaminhosCompleta, MaxLigacoes, Caminho, Custo)
	    )
	).

% Devolve o Primeiro elemento de uma Lista no primeiro argumento e devolve a lista sem esse elemento no terceiro argumento
primeiroElemento(PrimeiroElemento, [PrimeiroElemento|Lista], Lista).
primeiroElemento(PrimeiroElemento, [_|Lista], ListaFinal):-
    primeiroElemento(PrimeiroElemento, Lista, ListaFinal).

% Retira o Custo presente numa lista de tuplos onde [(Custo, Caminho)]
retiraCustos([],[]).
retiraCustos([(_,LA)|L],[LA|L1]):-
    retiraCustos(L,L1).

calculaCustoForcaLigacao([Atual,X], Custo):-
    !,
	ligacao(Atual, X, Custo, _, _, _).

calculaCustoForcaLigacao([Atual, X|L], Resultado):-
    calculaCustoForcaLigacao([X|L], Resultado1),
	ligacao(Atual, X, ForcaLigacao, _, _, _),
	Resultado is Resultado1 + ForcaLigacao.

% --------------------------------------------------------------


% --------------------------------------------------------------
% Best First Multi Criterio
bestFirstMultiCriterio(Origem, Destino, MaxLigacoes, Caminho, Custo):-
	get_time(TempoInicio),

	bestFirstMultiCriterio2(Destino, [[Origem]], MaxLigacoes, Caminho, Custo),

	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').


bestFirstMultiCriterio2(Destino, [[Destino|T]|_], MaxLigacoes, Caminho, Custo):-
	reverse([Destino|T], Caminho),
	length(Caminho, Temp),
	NumeroLigacoes is Temp - 1,
    NumeroLigacoes =< MaxLigacoes,
	calculaCustoMultiCriterio(Caminho, Custo).

bestFirstMultiCriterio2(Destino, [[Destino|_]|ListaCaminhos], MaxLigacoes, Caminho, Custo):-
	!,
	bestFirstMultiCriterio2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo).


bestFirstMultiCriterio2(Destino, ListaCaminhos, MaxLigacoes, Caminho, Custo):-
	primeiroElemento(CaminhoAtual, ListaCaminhos, ListaCaminhosAtualizada),
	CaminhoAtual = [Atual|_],
	(
	    (
		    Atual == Destino,
			!,
			bestFirstMultiCriterio2(Destino, [CaminhoAtual|ListaCaminhosAtualizada], MaxLigacoes, Caminho, Custo)
		)
	    ;
	    (
	        findall((FatorMultiCriterio, [X|CaminhoAtual]), (
			    ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	            \+member(X, CaminhoAtual),
				funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio)
			), Novos),
	        Novos \==[],
			!,
	        sort(0, @>=, Novos, NovosOrdenados),
	        retiraCustos(NovosOrdenados, NovosOrdenadosNormalizados),
	        append(NovosOrdenadosNormalizados, ListaCaminhosAtualizada, ListaCaminhosCompleta),
	        bestFirstMultiCriterio2(Destino, ListaCaminhosCompleta, MaxLigacoes, Caminho, Custo)
	    )
	).

calculaCustoMultiCriterio([Atual,X], Custo):-
    !,
	ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, Custo).

calculaCustoMultiCriterio([Atual, X|L], Resultado):-
    calculaCustoMultiCriterio([X|L], Resultado1),
	ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	Resultado is Resultado1 + FatorMultiCriterio.

% --------------------------------------------------------------


% --------------------------------------------------------------
% A* - Forca de Ligacao
aStarForcaLigacao(Origem, Destino, NumeroMaximoLigacoes, Caminho, Custo):-
    get_time(TempoInicio),

    aStarForcaLigacao2(Destino, [(_, 0, [Origem], [])], NumeroMaximoLigacoes, Caminho, Custo),
	get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	%format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

% A Lista Auxiliar encontra-se invertida porque os n�s s�o adicionados na cabe�a
aStarForcaLigacao2(Destino,[(_, Custo, [Destino|Tail], ListaLigacoes)|_], NumeroMaximoLigacoes, Caminho, Custo):-
	length(ListaLigacoes, NumeroLigacoes),
	NumeroLigacoes =< NumeroMaximoLigacoes,
    reverse([Destino|Tail], Caminho).

aStarForcaLigacao2(Destino,[(_, CustoAtual, CaminhoPercorridoAtual, ListaLigacoes)|Outros], NumeroMaximoLigacoes, Caminho, Custo):-
    CaminhoPercorridoAtual = [Atual|_],
    findall((CustoEstimadoX, CustoAteX, [X|CaminhoPercorridoAtual], [CustoX|ListaLigacoes]), (
        Destino \== Atual,									% Verifica se o N� Atual n�o � igual ao Destino
		ligacao(Atual, X, CustoX, _, _, _),				% Obtem o custo do N� Atual at� X
        \+ member(X, CaminhoPercorridoAtual),				% Verifica se X n�o pertence ao Caminho Atual
        estimativaForcaLigacao(X, ListaLigacoes, NumeroMaximoLigacoes, EstimativaX),	% EstimativaX - Calcula a estimativa desde X at� ao Destino
		CustoAteX is CustoX + CustoAtual,					% Custo at� X - igual ao CustoX + Custo Atual do caminho
        CustoEstimadoX is CustoAteX + EstimativaX			% Custo Estimado � igual a Custo Atual do caminho + Estimativa X
	), Novos),
    append(Outros, Novos, Todos),
    sort(0, @>=, Todos, TodosOrdenados),
    aStarForcaLigacao2(Destino, TodosOrdenados, NumeroMaximoLigacoes, Caminho, Custo).

estimativaForcaLigacao(No, ListaLigacoes, NumeroMaximoLigacoes, Estimativa):-
    length(ListaLigacoes, NumeroLigacoesAtual),
	Nivel is NumeroMaximoLigacoes - NumeroLigacoesAtual,
	forcasLigacaoUtilizador(No, Nivel, ListaForcas),
    sort(0, @>=, ListaForcas, ListaForcasOrdenada),
    removeForcasUtilizadas(ListaLigacoes, ListaForcasOrdenada, ListaForcasAtualizada),
    primeirosNElementos(ListaForcasAtualizada, Nivel, ListaForcasFinal),
	soma(ListaForcasFinal, Estimativa).

% Retorna as for�as de liga��o do Utilizador para determinado Nivel
forcasLigacaoUtilizador(Utilizador, Nivel, ListaForcas):-
	tamanhoRedeUtilizador(Utilizador, Nivel, ListaUtilizadoresRede, _),
	forcaLigacaoLista(ListaUtilizadoresRede, [], ListaForcas).

forcaLigacaoLista([], ListaFinal, ListaFinal):-
	!.

forcaLigacaoLista([Utilizador|ListaUtilizadoresRede], ListaLigacoesAtual, ListaFinal):-
    findall(ForcaLigacao, (
	    member(X, ListaUtilizadoresRede),
	    X \== Utilizador,
		forcaLigacao(Utilizador, X, ForcaLigacao)
    ),
	ListaLigacoes),

	append(ListaLigacoes, ListaLigacoesAtual, ListaLigacoesCompleta),

	forcaLigacaoLista(ListaUtilizadoresRede, ListaLigacoesCompleta, ListaFinal).


% For�a de liga��o entre X e Y
forcaLigacao(X,Y, ForcaLigacao):-
    ligacao(X,Y,ForcaLigacao,_,_,_),
	!.

forcaLigacao(X,Y,ForcaLigacao):-
    ligacao(Y,X,_,ForcaLigacao,_,_),
	!.

forcaLigacao(_,_, _):-
	fail,
	!.

% --------------------------------------------------------------


% --------------------------------------------------------------
% A* - Multi Criterio
aStarMultiCriterio(Origem, Destino, NumeroMaximoLigacoes, Caminho, Custo):-
	get_time(TempoInicio),
	
    aStarMultiCriterio2(Destino, [(_, 0, [Origem], [])], NumeroMaximoLigacoes, Caminho, Custo),get_time(TempoFim),

	TempoExecucao is TempoFim - TempoInicio,
	%format('~5f', TempoExecucao),

	write('Tempo de geracao da Solucao: '),
	write(TempoExecucao), write(' segundos').

% A Lista Auxiliar encontra-se invertida porque os n�s s�o adicionados na cabe�a
aStarMultiCriterio2(Destino,[(_, Custo, [Destino|Tail], ListaLigacoes)|_], NumeroMaximoLigacoes, Caminho, Custo):-
	length(ListaLigacoes, NumeroLigacoes),
	NumeroLigacoes =< NumeroMaximoLigacoes,
    reverse([Destino|Tail], Caminho).

aStarMultiCriterio2(Destino,[(_, CustoAtual, CaminhoPercorridoAtual, ListaLigacoes)|Outros], NumeroMaximoLigacoes, Caminho, Custo):-
    CaminhoPercorridoAtual = [Atual|_],
    findall((CustoEstimadoX, CustoAteX, [X|CaminhoPercorridoAtual], [CustoX|ListaLigacoes]), (
        Destino \== Atual,										% Verifica se o N� Atual n�o � igual ao Destino
		ligacao(Atual, X, ForcaLigacao, _, ForcaRelacao, _),	% Obtem o custo do N� Atual at� X
		\+ member(X, CaminhoPercorridoAtual),					% Verifica se X n�o pertence ao Caminho Atual
        funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, CustoX),
        estimativaMultiCriterio(X, ListaLigacoes, NumeroMaximoLigacoes, EstimativaX),	% EstimativaX - Calcula a estimativa desde X at� ao Destino
		CustoAteX is CustoX + CustoAtual,						% Custo at� X - igual ao CustoX + Custo Atual do caminho
        CustoEstimadoX is CustoAteX + EstimativaX				% Custo Estimado � igual a Custo Atual do caminho + Estimativa X
	), Novos),
    append(Outros, Novos, Todos),
    sort(0, @>=, Todos, TodosOrdenados),
    aStarMultiCriterio2(Destino, TodosOrdenados, NumeroMaximoLigacoes, Caminho, Custo).

estimativaMultiCriterio(No, ListaLigacoes, NumeroMaximoLigacoes, Estimativa):-
    length(ListaLigacoes, NumeroLigacoesAtual),
	Nivel is NumeroMaximoLigacoes - NumeroLigacoesAtual,
	forcasMultiCriterioUtilizador(No, Nivel, ListaForcas),
    sort(0, @>=, ListaForcas, ListaForcasOrdenada),
    removeForcasUtilizadas(ListaLigacoes, ListaForcasOrdenada, ListaForcasAtualizada),
    primeirosNElementos(ListaForcasAtualizada, Nivel, ListaForcasFinal),
	soma(ListaForcasFinal, Estimativa).

% Retorna as for�as de liga��o do Utilizador para determinado Nivel
forcasMultiCriterioUtilizador(Utilizador, Nivel, ListaForcas):-
	tamanhoRedeUtilizador(Utilizador, Nivel, ListaUtilizadoresRede, _),
	forcaMultiCriterioLista(ListaUtilizadoresRede, [], ListaForcas).

forcaMultiCriterioLista([], ListaFinal, ListaFinal):-
	!.

forcaMultiCriterioLista([Utilizador|ListaUtilizadoresRede], ListaLigacoesAtual, ListaFinal):-
    findall(FatorMultiCriterio, (
	    member(X, ListaUtilizadoresRede),
	    X \== Utilizador,
		forcaMultiCriterio(Utilizador, X, FatorMultiCriterio)
    ),
	ListaLigacoes),

	append(ListaLigacoes, ListaLigacoesAtual, ListaLigacoesCompleta),

	forcaMultiCriterioLista(ListaUtilizadoresRede, ListaLigacoesCompleta, ListaFinal).


% For�a Multi Criterio entre X e Y
forcaMultiCriterio(X, Y, FatorMultiCriterio):-
    ligacao(X, Y, ForcaLigacao, _, ForcaRelacao, _),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	!.

forcaMultiCriterio(X, Y, FatorMultiCriterio):-
    ligacao(Y, X, _, ForcaLigacao, _, ForcaRelacao),
	funcaoMultiCriterio(ForcaLigacao, ForcaRelacao, FatorMultiCriterio),
	!.

forcaMultiCriterio(_,_, _):-
	fail,
	!.

% --------------------------------------------------------------


% --------------------------------------------------------------
% AUXILIARES

% Verifica se existe liga��o entre dois Utilizadores - Unidirecional
connectUnidirecional(X,Y):-
    ligacao(X,Y,_,_,_,_).


% Apaga 1 Ocorrencia de X Em L e devolve L1 no 3� argumento
apaga1(_,[],[]):-
    !.

apaga1(X,[X|L],L):-
    !.

apaga1(X,[Y|L],[Y|L1]):-
    apaga1(X, L, L1).


% Soma de elementos de uma Lista
soma([], 0):-
    !.

soma([X|Lista], Total):-
    soma(Lista, Total1),
    Total is Total1 + X.


% Remove Elementos da Lista do 1� argumento que se encontram na lista do 2� argumento
removeForcasUtilizadas([], ListaForcas, ListaForcas):-
	!.

removeForcasUtilizadas([Ligacao|ListaLigacoes], ListaForcas, Final):-
	apaga1(Ligacao, ListaForcas, NovaLista),
	removeForcasUtilizadas(ListaLigacoes, NovaLista, Final).

primeirosNElementos([], _, []):-
    !.

primeirosNElementos(_, 0, []):-
    !.

primeirosNElementos([Cabeca|ListaForcasOrdenada], NumeroMaximoLigacoes, [Cabeca|ListaFinal]):-
    ContadorLigacoes is NumeroMaximoLigacoes - 1,
    primeirosNElementos(ListaForcasOrdenada, ContadorLigacoes, ListaFinal).


% --------------------------------------------------------------
% Tamanho da Rede de um Utilizador
tamanhoRedeUtilizador(Utilizador, Nivel, ListaFinal, Tamanho):-
    tamanhoRede2(Nivel,[Utilizador],[],[Utilizador],ListaFinal),
	length(ListaFinal, TamanhoListaFinal),
    % Retirar o primeiro Utilizador
    Tamanho is TamanhoListaFinal - 1,
	!.

% Quando as duas listas de queues se encontram vazias, j� n�o existem mais
% n�s a percorrer na rede
tamanhoRede2(_,[], [], ListaFinal, ListaFinal):-
    !.

% Quando a queue se encontra vazia, decrementa-se o n�vel e substitui-se
% a queue por todos os n�s visitados no n�vel anterior
tamanhoRede2(Nivel,[], ProxQueue, ListaFinal, ListaResultado):-
    NivelAtual is Nivel - 1,
    tamanhoRede2(NivelAtual, ProxQueue, [], ListaFinal, ListaResultado).

% Quando o n�vel chega a 0, o tamanho ser� a length da ListaFinal - 1
% para retirar o utilizador inicial
tamanhoRede2(0,_,_,ListaFinal,ListaFinal):-
    !.

tamanhoRede2(Nivel, [Utilizador|QueueUtilizadores], ProximaQueue, ListaFinal, ListaResultado):-
    % Preencher ListaUserNivel com todos os X
    % X ser� utilizadores conectados ao primeiro Utilizador na queue e que n�o
    % se encontrem na Lista Final
    findall(X,(
                connectUnidirecional(Utilizador, X),
                % \+member(X, [Utilizador|QueueUtilizadores]),
                \+member(X, ListaFinal)
            ),
            ListaUserNivel),

    % Adicionar a lista de n�s visitados � Lista Final
    append(ListaUserNivel, ListaFinal, ListaCompleta),

    % write(ListaCompleta),nl,

    % Criar lista de n�s visitados sem n�s que j� se encontrem em queue
    findall(Y,(
                member(Y, ListaUserNivel),
                \+member(Y, ProximaQueue)
            ), UserNivelSemRepetidos),

    % Adicionar os n�s visitados que ainda n�o est�o na queue � proxima queue
    append(UserNivelSemRepetidos, ProximaQueue, NovaQueue),

    % Chamada Recursiva
    tamanhoRede2(Nivel, QueueUtilizadores, NovaQueue, ListaCompleta, ListaResultado).

% --------------------------------------------------------------


% --------------------------------------------------------------
% Primeiro em Profundidade Caminho Mais Forte N Ligacoes - Unidirecional
dfsCaminhoMaisForteNLigacoes1(Origem, Destino, NumeroMaxLigacoes, Caminho):-
    dfsCaminhoMaisForteNLigacoes2(Origem, Destino, [Origem], NumeroMaxLigacoes, Caminho).

% Condi��o Final: User Atual = Destino
dfsCaminhoMaisForteNLigacoes2(Destino, Destino, ListaAux, _, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os n�s s�o adicionados na cabe�a
    !, reverse(ListaAux, Caminho).


dfsCaminhoMaisForteNLigacoes2(UserAtual, Destino, ListaAux, NumeroMaxLigacoes, Caminho):-
	length(ListaAux, LengthCaminho),
	LengthCaminho =< NumeroMaxLigacoes,

    % Testar Liga��o entre NoAtual e qualquer X
    connectUnidirecional(UserAtual, X),

    % Testar se X j� pertence � Lista para n�o visitar n�s j� visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsCaminhoMaisForteNLigacoes2(X, Destino, [X|ListaAux], NumeroMaxLigacoes, Caminho).
