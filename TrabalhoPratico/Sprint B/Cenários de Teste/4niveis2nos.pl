% Base de Conhecimento - 4 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
% Nivel 4
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto]).
no(42,diogo,[natureza,futebol,sw,jogos,porto]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
% Nivel 2
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).
% Nivel 3
ligacao(31,41,2,4).
ligacao(31,42,6,3).
ligacao(32,41,2,3).
ligacao(32,42,-1,0).

ligacao(41,200,3,8).
ligacao(42,200,1,-3).

% Caminho Adicional
ligacao(1,200,3,5).