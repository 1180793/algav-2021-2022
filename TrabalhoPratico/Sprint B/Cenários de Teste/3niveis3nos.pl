% Base de Conhecimento - 3 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
no(13,carlos,[natureza,musica,sw,futebol,coimbra]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa]).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
no(33,andre,[natureza,carros,futebol,coimbra]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).
ligacao(1,13,-3,-2).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(11,23,-2,8).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
ligacao(12,23,2,4).
ligacao(13,21,3,2).
ligacao(13,22,0,-3).
ligacao(13,23,5,9).
% Nivel 2
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(21,33,3,5).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).
ligacao(22,33,2,1).
ligacao(23,31,4,-3).
ligacao(23,32,3,5).
ligacao(23,33,4,1).

ligacao(31,200,3,8).
ligacao(32,200,1,-3).
ligacao(33,200,5,-1).

% Caminho Adicional
ligacao(1,200,3,5).