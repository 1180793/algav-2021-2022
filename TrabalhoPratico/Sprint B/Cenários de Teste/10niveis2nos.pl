% Base de Conhecimento - 10 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
% Nivel 4
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto]).
no(42,diogo,[natureza,futebol,sw,jogos,porto]).
% Nivel 5
no(51,rodolfo,[natureza,musica,sw]).
no(52,joana,[natureza,fitness,viagens,lisboa]).
% Nivel 6
no(61,pilar,[fitness,viagens,natureza]).
no(62,miguel,[cinema,futebol,fotografia]).
% Nivel 7
no(71,gabriel,[literatura,culinaria,viagens,braga]).
no(72,goncalo,[moda,cinema,fotografia,aveiro]).
% Nivel 8
no(81,mateus,[culinaria,financas,viagens]).
no(82,diana,[carros,futebol,literatura,aveiro]).
% Nivel 9
no(91,samuel,[moda,futebol,viagens,pintura]).
no(92,renato,[natureza,literatura,fitness]).
% Nivel 10
no(101,cristina,[musica,culinaria,financas,coimbra]).
no(102,cristiano,[futebol,moda,jogos,porto]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).

% Nivel 2
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).

% Nivel 3
ligacao(31,41,2,4).
ligacao(31,42,6,3).
ligacao(32,41,2,3).
ligacao(32,42,-1,0).

% Nivel 4
ligacao(41,51,8,-2).
ligacao(41,52,6,-1).
ligacao(42,51,5,-1).
ligacao(42,52,6,5).

% Nivel 5
ligacao(51,61,4,9).
ligacao(51,62,10,-2).
ligacao(52,61,5,9).
ligacao(52,62,3,9).

% Nivel 6
ligacao(61,71,-3,-3).
ligacao(61,72,6,4).
ligacao(62,71,1,6).
ligacao(62,72,9,3).

% Nivel 7
ligacao(71,81,-2,5).
ligacao(71,82,-3,1).
ligacao(72,81,5,-3).
ligacao(72,82,-2,0).

% Nivel 8
ligacao(81,91,-2,-2).
ligacao(81,92,-2,9).
ligacao(82,91,8,-4).
ligacao(82,92,4,2).

% Nivel 9
ligacao(91,101,-1,-1).
ligacao(91,102,5,-2).
ligacao(92,101,3,7).
ligacao(92,102,2,3).

ligacao(101,200,3,8).
ligacao(102,200,1,-3).

% Caminho Adicional
ligacao(1,199,3,5).
ligacao(199,200,4,3).