% Base de Conhecimento - 5 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
no(13,carlos,[natureza,musica,sw,futebol,coimbra]).
no(14,daniel,[natureza,cinema,jogos,sw,moda]).
no(15,joao,[natureza,carros,tecnologia,porto]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa]).
no(24,luisa,[natureza,cinema,jogos,moda,porto]).
no(25,gaspar,[teatro,moda,jogos,lisboa]).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
no(33,andre,[natureza,carros,futebol,coimbra]).
no(34,catia,[natureza,musica,cinema,lisboa,moda]).
no(35,tiago,[natureza,musica,pintura,carros,coimbra]).
% Nivel 4
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto]).
no(42,diogo,[natureza,futebol,sw,jogos,porto]).
no(43,ernesto,[natureza,teatro,carros,porto]).
no(44,isaura,[natureza,moda,tecnologia,cinema]).
no(45,nurio,[viagens,pintura,literatura,aveiro]).
% Nivel 5
no(51,rodolfo,[natureza,musica,sw]).
no(52,joana,[natureza,fitness,viagens,lisboa]).
no(53,leticia,[natureza,financas,literatura,porto]).
no(54,antonia,[tecnologia,literatura,fitness,braga]).
no(55,mariana,[viagens,teatro,carros,setubal]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).
ligacao(1,13,-3,-2).
ligacao(1,14,1,-5).
ligacao(1,15,-4,6).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(11,23,-2,8).
ligacao(11,24,6,0).
ligacao(11,25,3,0).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
ligacao(12,23,2,4).
ligacao(12,24,-2,4).
ligacao(12,25,3,0).
ligacao(13,21,3,2).
ligacao(13,22,0,-3).
ligacao(13,23,5,9).
ligacao(13,24,-2, 4).
ligacao(13,25,6,8).
ligacao(14,21,2,6).
ligacao(14,22,6,-3).
ligacao(14,23,7,0).
ligacao(14,24,2,2).
ligacao(14,25,-4,9).
ligacao(15,21,1,-3).
ligacao(15,22,1,9).
ligacao(15,23,-1,5).
ligacao(15,24,0,-2).
ligacao(15,25,9,1).
% Nivel 2
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(21,33,3,5).
ligacao(21,34,4,2).
ligacao(21,35,8,-2).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).
ligacao(22,33,2,1).
ligacao(22,34,2,3).
ligacao(22,35,9,7).
ligacao(23,31,4,-3).
ligacao(23,32,3,5).
ligacao(23,33,4,1).
ligacao(23,34,-2,-3).
ligacao(23,35,1,9).
ligacao(24,31,1,-5).
ligacao(24,32,1,0).
ligacao(24,33,3,-1).
ligacao(24,34,-1,5).
ligacao(24,35,9,1).
ligacao(25,31,9,-2).
ligacao(25,32,5,4).
ligacao(25,33,-3,3).
ligacao(25,34,5,4).
ligacao(25,35,5,2).
% Nivel 3
ligacao(31,41,2,4).
ligacao(31,42,6,3).
ligacao(31,43,2,1).
ligacao(31,44,2,1).
ligacao(31,45,-2,1).
ligacao(32,41,2,3).
ligacao(32,42,-1,0).
ligacao(32,43,0,1).
ligacao(32,44,1,2).
ligacao(32,45,-2,-4).
ligacao(33,41,4,-1).
ligacao(33,42,-1,3).
ligacao(33,43,7,2).
ligacao(33,44,5,-3).
ligacao(33,45,0,2).
ligacao(34,41,3,2).
ligacao(34,42,1,-1).
ligacao(34,43,2,4).
ligacao(34,44,1,-2).
ligacao(34,45,4,9).
ligacao(35,41,8,-4).
ligacao(35,42,8,2).
ligacao(35,43,6,8).
ligacao(35,44,7,4).
ligacao(35,45,-1,4).
% Nivel 4
ligacao(41,51,8,-2).
ligacao(41,52,6,-1).
ligacao(41,53,9,2).
ligacao(41,54,-1,-4).
ligacao(41,55,0,9).
ligacao(42,51,5,-1).
ligacao(42,52,6,5).
ligacao(42,53,3,0).
ligacao(42,54,8,3).
ligacao(42,55,3,9).
ligacao(43,51,1,3).
ligacao(43,52,10,-1).
ligacao(43,53,4,0).
ligacao(43,54,3,6).
ligacao(43,55,5,3).
ligacao(44,51,4,2).
ligacao(44,52,-3,9).
ligacao(44,53,3,-2).
ligacao(44,54,2,3).
ligacao(44,55,-1,3).
ligacao(45,51,-4,10).
ligacao(45,52,7,7).
ligacao(45,53,-3,1).
ligacao(45,54,-1,8).
ligacao(45,55,5,9).

ligacao(51,200,3,8).
ligacao(52,200,1,-3).
ligacao(53,200,5,-1).
ligacao(54,200,-2,-3).
ligacao(55,200,2,10).

% Caminho Adicional
ligacao(1,199,3,5).
ligacao(199,200,4,3).