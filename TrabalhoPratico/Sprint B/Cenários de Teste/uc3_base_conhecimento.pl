% Base de Conhecimento

% no(id, nome, [tags de temas de interesse])

no(1,ana,[natureza,pintar,musica,sw,porto, teste]).

no(11,antonio,[pintura]).
no(12,beatriz,[]).
no(13,carlos,[]).
no(14,daniel,[]).

no(21,eduardo,[]).
no(22,isabel,[por]).
no(23,jose,[paint]).
no(24,luisa,[]).

no(31,maria,[]).
no(32,anabela,[]).
no(33,andre,[musica]).
no(34,catia,[musica]).

no(41,cesar,[]).
no(42,diogo,[porto]).
no(43,ernesto,[pintura]).
no(44,isaura,[]).

no(51,rodolfo,[]).
no(61,rita,[natureza,moda,mus,sw,coimbra]).

no(200,sara,[natureza,moda,musica,sw,coimbra]).



% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100

% Nivel 1
ligacao(1,11,10,8).
ligacao(1,12,2,6).
ligacao(1,13,-3,-2).
ligacao(1,14,1,-5).

% Nivel 2
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(11,23,-2,8).
ligacao(11,24,6,0).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
ligacao(12,23,2,4).
ligacao(12,24,-2,4).
ligacao(13,21,3,2).
ligacao(13,22,0,-3).
ligacao(13,23,5,9).
ligacao(13,24,-2, 4).
ligacao(14,21,2,6).
ligacao(14,22,6,-3).
ligacao(14,23,7,0).
ligacao(14,24,2,2).
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(21,33,3,5).
ligacao(21,34,4,2).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).
ligacao(22,33,2,1).
ligacao(22,34,2,3).
ligacao(23,31,4,-3).
ligacao(23,32,3,5).
ligacao(23,33,4,1).
ligacao(23,34,-2,-3).
ligacao(24,31,1,-5).
ligacao(24,32,1,0).
ligacao(24,33,3,-1).
ligacao(24,34,-1,5).
ligacao(31,200,2,0).
ligacao(32,200,7,-2).
ligacao(33,200,-2,4).
ligacao(34,200,-1,-3).

ligacao(1,51,6,2).
ligacao(51,61,7,3).
ligacao(61,200,2,4).