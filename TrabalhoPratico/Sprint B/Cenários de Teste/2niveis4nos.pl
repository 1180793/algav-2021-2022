% Base de Conhecimento - 4 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
no(13,carlos,[natureza,musica,sw,futebol,coimbra]).
no(14,daniel,[natureza,cinema,jogos,sw,moda]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa]).
no(24,luisa,[natureza,cinema,jogos,moda,porto]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).
ligacao(1,13,-3,-2).
ligacao(1,14,1,-5).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(11,23,-2,8).
ligacao(11,24,6,0).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
ligacao(12,23,2,4).
ligacao(12,24,-2,4).
ligacao(13,21,3,2).
ligacao(13,22,0,-3).
ligacao(13,23,5,9).
ligacao(13,24,-2, 4).
ligacao(14,21,2,6).
ligacao(14,22,6,-3).
ligacao(14,23,7,0).
ligacao(14,24,2,2).

ligacao(21,200,3,8).
ligacao(22,200,1,-3).
ligacao(23,200,5,-1).
ligacao(24,200,-2,-3).

% Caminho Adicional
ligacao(1,200,3,5).