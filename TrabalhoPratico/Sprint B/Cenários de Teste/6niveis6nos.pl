% Base de Conhecimento - 6 Niveis Intermédios

% no(id, nome, [tags de temas de interesse])

% No Inicial
no(1,ana,[natureza,pintura,musica,sw,porto]).

% Nivel 1
no(11,antonio,[natureza,pintura,carros,futebol,lisboa]).
no(12,beatriz,[natureza,musica,carros,porto,moda]).
no(13,carlos,[natureza,musica,sw,futebol,coimbra]).
no(14,daniel,[natureza,cinema,jogos,sw,moda]).
no(15,joao,[natureza,carros,tecnologia,porto]).
no(16,fernando,[natureza,teatro,tecnologia,futebol,lisboa]).
% Nivel 2
no(21,eduardo,[natureza,cinema,teatro,carros,coimbra]).
no(22,isabel,[natureza,musica,porto,lisboa,cinema]).
no(23,jose,[natureza,pintura,sw,musica,carros,lisboa]).
no(24,luisa,[natureza,cinema,jogos,moda,porto]).
no(25,gaspar,[teatro,moda,jogos,lisboa]).
no(26,julio,[viagens,literatura,natureza,porto]).
% Nivel 3
no(31,maria,[natureza,pintura,musica,moda,porto]).
no(32,anabela,[natureza,cinema,musica,tecnologia,porto]).
no(33,andre,[natureza,carros,futebol,coimbra]).
no(34,catia,[natureza,musica,cinema,lisboa,moda]).
no(35,tiago,[natureza,musica,pintura,carros,coimbra]).
no(36,leandro,[literatura,moda,cinema,setubal]).
% Nivel 4
no(41,cesar,[natureza,teatro,tecnologia,futebol,porto]).
no(42,diogo,[natureza,futebol,sw,jogos,porto]).
no(43,ernesto,[natureza,teatro,carros,porto]).
no(44,isaura,[natureza,moda,tecnologia,cinema]).
no(45,nurio,[viagens,pintura,literatura,aveiro]).
no(46,hugo,[jogos,carros,financas,teatro]).
% Nivel 5
no(51,rodolfo,[natureza,musica,sw]).
no(52,joana,[natureza,fitness,viagens,lisboa]).
no(53,leticia,[natureza,financas,literatura,porto]).
no(54,antonia,[tecnologia,literatura,fitness,braga]).
no(55,mariana,[viagens,teatro,carros,setubal]).
no(56,pedro,[cinema,natureza,culinaria]).
% Nivel 6
no(61,pilar,[fitness,viagens,natureza]).
no(62,miguel,[cinema,futebol,fotografia]).
no(63,henrique,[teatro,moda,musica,financas]).
no(64,clara,[tecnologia,financas,carros]).
no(65,santiago,[culinaria,literatura,pintura]).
no(66,salvador,[desporto,jogos,fitness]).

% Caminho Adicional
no(199,nuno,[natureza,lisboa,cinema,moda]).
no(200,sara,[natureza,moda,musica,sw,coimbra]).

% ligacao(idPessoa1, idPessoa2, forcaLigacaoEntre1e2, forcaLigacaoEntre2e1) - Posteriormente as forças poderam ser postas entre 0 e 100
ligacao(1,11,10,8).
ligacao(1,12,2,6).
ligacao(1,13,-3,-2).
ligacao(1,14,1,-5).
ligacao(1,15,-4,6).
ligacao(1,16,-2,8).

% Nivel 1
ligacao(11,21,5,7).
ligacao(11,22,2,-4).
ligacao(11,23,-2,8).
ligacao(11,24,6,0).
ligacao(11,25,3,0).
ligacao(11,26,8,9).
ligacao(12,21,4,9).
ligacao(12,22,-3,-8).
ligacao(12,23,2,4).
ligacao(12,24,-2,4).
ligacao(12,25,3,0).
ligacao(12,26,8,9).
ligacao(13,21,3,2).
ligacao(13,22,0,-3).
ligacao(13,23,5,9).
ligacao(13,24,-2, 4).
ligacao(13,25,6,8).
ligacao(13,26,8,-2).
ligacao(14,21,2,6).
ligacao(14,22,6,-3).
ligacao(14,23,7,0).
ligacao(14,24,2,2).
ligacao(14,25,-4,9).
ligacao(14,26,9,7).
ligacao(15,21,1,-3).
ligacao(15,22,1,9).
ligacao(15,23,-1,5).
ligacao(15,24,0,-2).
ligacao(15,25,9,1).
ligacao(15,26,7,-3).
ligacao(16,21,8,5).
ligacao(16,22,1,-4).
ligacao(16,23,-2,-4).
ligacao(16,24,9,-2).
ligacao(16,25,5,4).
ligacao(16,26,-3,3).
% Nivel 2
ligacao(21,31,2,1).
ligacao(21,32,-2,3).
ligacao(21,33,3,5).
ligacao(21,34,4,2).
ligacao(21,35,8,-2).
ligacao(21,36,3,9).
ligacao(22,31,5,-4).
ligacao(22,32,-1,6).
ligacao(22,33,2,1).
ligacao(22,34,2,3).
ligacao(22,35,9,7).
ligacao(22,36,10,5).
ligacao(23,31,4,-3).
ligacao(23,32,3,5).
ligacao(23,33,4,1).
ligacao(23,34,-2,-3).
ligacao(23,35,1,9).
ligacao(23,36,-1,5).
ligacao(24,31,1,-5).
ligacao(24,32,1,0).
ligacao(24,33,3,-1).
ligacao(24,34,-1,5).
ligacao(24,35,9,1).
ligacao(24,36,3,6).
ligacao(25,31,9,-2).
ligacao(25,32,5,4).
ligacao(25,33,-3,3).
ligacao(25,34,5,4).
ligacao(25,35,5,2).
ligacao(25,36,5,0).
ligacao(26,31,2,6).
ligacao(26,32,6,-2).
ligacao(26,33,7,-3).
ligacao(26,34,-4,1).
ligacao(26,35,4,5).
ligacao(26,36,3,-3).
% Nivel 3
ligacao(31,41,2,4).
ligacao(31,42,6,3).
ligacao(31,43,2,1).
ligacao(31,44,2,1).
ligacao(31,45,-2,1).
ligacao(31,46,-3,2).
ligacao(32,41,2,3).
ligacao(32,42,-1,0).
ligacao(32,43,0,1).
ligacao(32,44,1,2).
ligacao(32,45,-2,-4).
ligacao(32,46,7,8).
ligacao(33,41,4,-1).
ligacao(33,42,-1,3).
ligacao(33,43,7,2).
ligacao(33,44,5,-3).
ligacao(33,45,0,2).
ligacao(33,46,-2,1).
ligacao(34,41,3,2).
ligacao(34,42,1,-1).
ligacao(34,43,2,4).
ligacao(34,44,1,-2).
ligacao(34,45,4,9).
ligacao(34,46,7,-3).
ligacao(35,41,8,-4).
ligacao(35,42,8,2).
ligacao(35,43,6,8).
ligacao(35,44,7,4).
ligacao(35,45,-1,4).
ligacao(35,46,-2,0).
ligacao(36,41,0,10).
ligacao(36,42,4,-3).
ligacao(36,43,7,-4).
ligacao(36,44,0,1).
ligacao(36,45,10,5).
ligacao(36,46,2,-4).
% Nivel 4
ligacao(41,51,8,-2).
ligacao(41,52,6,-1).
ligacao(41,53,9,2).
ligacao(41,54,-1,-4).
ligacao(41,55,0,9).
ligacao(41,56,6,6).
ligacao(42,51,5,-1).
ligacao(42,52,6,5).
ligacao(42,53,3,0).
ligacao(42,54,8,3).
ligacao(42,55,3,9).
ligacao(42,56,-2,3).
ligacao(43,51,1,3).
ligacao(43,52,10,-1).
ligacao(43,53,4,0).
ligacao(43,54,3,6).
ligacao(43,55,5,3).
ligacao(43,56,6,-3).
ligacao(44,51,4,2).
ligacao(44,52,-3,9).
ligacao(44,53,3,-2).
ligacao(44,54,2,3).
ligacao(44,55,-1,3).
ligacao(44,56,3,5).
ligacao(45,51,-4,10).
ligacao(45,52,7,7).
ligacao(45,53,-3,1).
ligacao(45,54,-1,8).
ligacao(45,55,5,9).
ligacao(45,56,6,-4).
ligacao(46,51,-1,9).
ligacao(46,52,1,9).
ligacao(46,53,-1,3).
ligacao(46,54,3,-4).
ligacao(46,55,-3,10).
ligacao(46,56,-2,-2).
% Nivel 5
ligacao(51,61,4,9).
ligacao(51,62,10,-2).
ligacao(51,63,2,10).
ligacao(51,64,1,6).
ligacao(51,65,5,2).
ligacao(51,66,5,5).
ligacao(52,61,5,9).
ligacao(52,62,3,9).
ligacao(52,63,8,7).
ligacao(52,64,1,6).
ligacao(52,65,-2,9).
ligacao(52,66,3,-1).
ligacao(53,61,1,-1).
ligacao(53,62,2,4).
ligacao(53,63,9,3).
ligacao(53,64,4,5).
ligacao(53,65,3,6).
ligacao(53,66,-2,3).
ligacao(54,61,6,9).
ligacao(54,62,9,-3).
ligacao(54,63,-3,-3).
ligacao(54,64,3,3).
ligacao(54,65,8,9).
ligacao(54,66,4,6).
ligacao(55,61,-3,-4).
ligacao(55,62,8,-2).
ligacao(55,63,0,-1).
ligacao(55,64,-2,1).
ligacao(55,65,4,-4).
ligacao(55,66,-4,6).
ligacao(56,61,2,-2).
ligacao(56,62,-1,8).
ligacao(56,63,2,5).
ligacao(56,64,2,2).
ligacao(56,65,-2,-2).
ligacao(56,66,8,9).

ligacao(61,200,3,8).
ligacao(62,200,1,-3).
ligacao(63,200,5,-1).
ligacao(64,200,-2,-3).
ligacao(65,200,2,10).
ligacao(66,200,8,4).

% Caminho Adicional
ligacao(1,199,3,5).
ligacao(199,200,4,3).