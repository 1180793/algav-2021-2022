% Sprint B

% --------------------------------------------------------------
% Auxiliares
% --------------------------------------------------------------

% Verifica se existe ligação entre dois Utilizadores - Unidirecional
connectUnidirecional(X,Y):-
    ligacao(X,Y,_,_).

% Verifica se existe ligação entre dois Utilizadores - Bidirecional
connectBidirecional(X,Y):-
    ligacao(X,Y,_,_);
    ligacao(Y,X,_,_).

% Imprime uma Lista com um Elemento por Linha
imprimirLista([]):-
	!.

imprimirLista([X|Lista]):-
	write(X),
	nl,
	imprimirLista(Lista).

% Imprime uma Lista com 2 Elementos por Linha
imprimirLista2Elementos([]):-
	!.

imprimirLista2Elementos([[Tags|Utilizadores]|Lista]):-
	write(Tags), write(' -> '),
	imprimirSequencia(Utilizadores),
	nl,
	imprimirLista2Elementos(Lista).

imprimirSequencia([Cabeca|Lista]):-
	length(Lista, LengthLista),
	LengthLista == 0,
	!,
	write(Cabeca),
	!.

imprimirSequencia([Cabeca|Lista]):-
	write(Cabeca), write(', '),
	imprimirSequencia(Lista).

% Intersecao de duas Listas
intersecao([],_,[]):-
    !.

intersecao([X|L1], L2, [X|ListaIntersecao]):-
    member(X, L2),
    !,
    intersecao(L1, L2, ListaIntersecao).

intersecao([_|L1], L2, ListaIntersecao):-
    intersecao(L1, L2, ListaIntersecao).

% Remove Elementos Repetidos de uma Lista
removeRepetidos([], []):-
	!.

removeRepetidos([X|Lista], ListaFinal):-
	member(X, Lista),
	!,
	removeRepetidos(Lista, ListaFinal).

removeRepetidos([X|Lista], [X|ListaFinal]):-
	removeRepetidos(Lista, ListaFinal).

% Normaliza Lista de Tags
normalizarSinonimos([], []):-
	!.

normalizarSinonimos([Tag|ListaTags], [Sinonimo|ListaFinal]):-
	sinonimo(Sinonimo, Tag),
	!,
	normalizarSinonimos(ListaTags, ListaFinal).

normalizarSinonimos([Tag|ListaTags], [Tag|ListaFinal]):-
	normalizarSinonimos(ListaTags, ListaFinal).

% --------------------------------------------------------------
% Primeiro em Profundidade - Unidirecional
dfsUnidirecional(Origem, Destino, Caminho):-
    dfsUnidirecional2(Origem, Destino, [Origem], Caminho).

% Condição Final: User Atual = Destino
dfsUnidirecional2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsUnidirecional2(UserAtual, Destino, ListaAux, Caminho):-
    % Testar Ligação entre NoAtual e qualquer X
    connectUnidirecional(UserAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsUnidirecional2(X, Destino, [X|ListaAux], Caminho).

% --------------------------------------------------------------
% Primeiro em Profundidade - Bidirecional
dfsBidirecional(Origem, Destino, Caminho):-
    dfsBidirecional2(Origem, Destino, [Origem], Caminho).

% Condição Final: User Atual = Destino
dfsBidirecional2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsBidirecional2(UserAtual, Destino, ListaAux, Caminho):-
    % Testar Ligação entre NoAtual e qualquer X
    connectBidirecional(UserAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsBidirecional2(X, Destino, [X|ListaAux], Caminho).

% --------------------------------------------------------------
% Todos os Caminhos
all_dfs(Origem, Destino, ListaCaminhos):-
    get_time(TempoInicio),

    findall(Caminho, dfsBidirecional(Origem, Destino, Caminho), ListaCaminhos),
    length(ListaCaminhos, NumeroSolucoes),

    get_time(TempoFim),

    write('Foram encontradas '), write(NumeroSolucoes), write(' solucoes em '),
    TempoExecucao is TempoFim - TempoInicio,
    write(TempoExecucao), write(' segundos'),nl,
    write('Lista de Caminhos Possíveis: '),
    write(ListaCaminhos),
    nl,nl.

% --------------------------------------------------------------
% Contador de Soluções
:- dynamic contadorSolucoes/1.

% --------------------------------------------------------------
% Primeiro em Profundidade Minimo Ligacoes (UC37) - Unidirecional
dfsMinimoLigacoesUnidirecional(Origem, Destino, Caminho):-
    dfsMinimoLigacoesUnidirecional2(Origem, Destino, [Origem], Caminho).

% Condição Final: User Atual = Destino
dfsMinimoLigacoesUnidirecional2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsMinimoLigacoesUnidirecional2(UserAtual, Destino, ListaAux, Caminho):-
	% Valorização relativamente à determinação do caminho mais curto
	% Se durante a aplicação do método chegarmos a conclusão que um
	% dado caminho já tem dimensão igual ou superior ao caminho mais
	% curto encontrado até esse momento não faz sentido continuar a
	% explorá-lo
	melhorSolucaoMinimoLigacoes(_, LengthMelhorAtual),
	length(ListaAux, LengthCaminho),
	LengthCaminho < LengthMelhorAtual,

    % Testar Ligação entre NoAtual e qualquer X
    connectUnidirecional(UserAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsMinimoLigacoesUnidirecional2(X, Destino, [X|ListaAux], Caminho).

% Primeiro em Profundidade Minimo Ligacoes (UC37) - Bidirecional
dfsMinimoLigacoesBidirecional(Origem, Destino, Caminho):-
    dfsMinimoLigacoesBidirecional2(Origem, Destino, [Origem], Caminho).

% Condição Final: User Atual = Destino
dfsMinimoLigacoesBidirecional2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsMinimoLigacoesBidirecional2(UserAtual, Destino, ListaAux, Caminho):-
	% Valorização relativamente à determinação do caminho mais curto
	% Se durante a aplicação do método chegarmos a conclusão que um
	% dado caminho já tem dimensão igual ou superior ao caminho mais
	% curto encontrado até esse momento não faz sentido continuar a
	% explorá-lo
	melhorSolucaoMinimoLigacoes(_, LengthMelhorAtual),
	length(ListaAux, LengthCaminho),
	LengthCaminho < LengthMelhorAtual,

    % Testar Ligação entre NoAtual e qualquer X
    connectBidirecional(UserAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsMinimoLigacoesBidirecional2(X, Destino, [X|ListaAux], Caminho).

% --------------------------------------------------------------
% Primeiro em Profundidade para Tags em Comum (UC35)
% Todos os nós tem que pertencer a Rede
dfsTagsComum(Origem, Destino, Tags, Caminho):-
    dfsTagsComum2(Origem, Destino, [Origem], Caminho),
	%write('Caminho: '),write(Caminho),nl,
    validaCaminhoTags(Caminho, Tags),
	!.

% Condição Final: User Atual = Destino
dfsTagsComum2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    !, reverse(ListaAux, Caminho).


dfsTagsComum2(UserAtual, Destino, ListaAux, Caminho):-
    % Testar Ligação entre NoAtual e qualquer X
    connectUnidirecional(UserAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfsTagsComum2(X, Destino, [X|ListaAux], Caminho).


% --------------------------------------------------------------
% UC34 - Tamanho da Rede de um Utilizador até determinado Nivel
% --------------------------------------------------------------

tamanhoRedeUtilizador(Utilizador, Nivel, ListaFinal, Tamanho):-
    tamanhoRede2(Nivel,[Utilizador],[],[Utilizador],ListaFinal),
	length(ListaFinal, TamanhoListaFinal),
    % Retirar o primeiro Utilizador
    Tamanho is TamanhoListaFinal - 1,
	!.

% Quando as duas listas de queues se encontram vazias, já não existem mais
% nós a percorrer na rede
tamanhoRede2(_,[], [], ListaFinal, ListaFinal):-
    !.

% Quando a queue se encontra vazia, decrementa-se o nível e substitui-se
% a queue por todos os nós visitados no nível anterior
tamanhoRede2(Nivel,[], ProxQueue, ListaFinal, ListaResultado):-
    NivelAtual is Nivel - 1,
    tamanhoRede2(NivelAtual, ProxQueue, [], ListaFinal, ListaResultado).

% Quando o nível chega a 0, o tamanho será a length da ListaFinal - 1
% para retirar o utilizador inicial
tamanhoRede2(0,_,_,ListaFinal,ListaFinal):-
    !.

tamanhoRede2(Nivel, [Utilizador|QueueUtilizadores], ProximaQueue, ListaFinal, ListaResultado):-
    % Preencher ListaUserNivel com todos os X
    % X será utilizadores conectados ao primeiro Utilizador na queue e que não
    % se encontrem na Lista Final
    findall(X,(
                connectBidirecional(Utilizador, X),
                % \+member(X, [Utilizador|QueueUtilizadores]),
                \+member(X, ListaFinal)
            ),
            ListaUserNivel),

    % Adicionar a lista de nós visitados à Lista Final
    append(ListaUserNivel, ListaFinal, ListaCompleta),

    % write(ListaCompleta),nl,

    % Criar lista de nós visitados sem nós que já se encontrem em queue
    findall(Y,(
                member(Y, ListaUserNivel),
                \+member(Y, ProximaQueue)
            ), UserNivelSemRepetidos),

    % Adicionar os nós visitados que ainda não estão na queue à proxima queue
    append(UserNivelSemRepetidos, ProximaQueue, NovaQueue),

    % Chamada Recursiva
    tamanhoRede2(Nivel, QueueUtilizadores, NovaQueue, ListaCompleta, ListaResultado).


% --------------------------------------------------------------
% UC35 - Utilizadores com X Tags em Comum
% --------------------------------------------------------------

% Utilizadores com X Tags em Comum
utilizadoresXTagsEmComum(X, ListaUtilizadores):-
	findall(Tag,
		(
			no(_,_,ListaTagsRaw),
			normalizarSinonimos(ListaTagsRaw, ListaTags),
			member(Tag,ListaTags)
		),
	ListaTodasTagsRepetidos),
	removeRepetidos(ListaTodasTagsRepetidos, ListaTodasTags),
	todasCombinacoes(X, ListaTodasTags, ListaUtilizadores),
	imprimirLista2Elementos(ListaUtilizadores).

% Lista com todas as combinações de X tags
todasCombinacoes(X, ListaTags,ListaCombinacaoXTags):-
	findall([CombinacaoTags, ListaUsers],
		(
			combinacao(X, ListaTags, CombinacaoTags),
			findall(UtilizadorY, (
				no(UtilizadorY,_,_),
				temTags(UtilizadorY, CombinacaoTags)
			), ListaUsers)
		),
	ListaCombinacaoXTags).

combinacao(0, _, []):-
	!.
combinacao(X, [Tag|ListaTags], [Tag|CombinacaoTags]):-
	X1 is X-1,
	combinacao(X1, ListaTags, CombinacaoTags).

combinacao(X, [_|ListaTags], CombinacaoTags):-
	combinacao(X, ListaTags, CombinacaoTags).

% Verifica se um Utilizador tem todas as tags da Lista de Tags
temTags(Utilizador, ListaTags):-
	no(Utilizador, _, TagsUtilizadorRaw),
	normalizarSinonimos(TagsUtilizadorRaw, TagsUtilizador),
	temTags2(TagsUtilizador, ListaTags).

temTags2(_, []):-
	!, true.

temTags2(TagsUtilizador, [Tag|ListaTags]):-
	member(Tag, TagsUtilizador),
	!,
	temTags2(TagsUtilizador, ListaTags).

temTags2(_, _):-
	fail.

% --------------------------------------------------------------
% UC36 - Sugerir Conexões com outros Utilizadores
% --------------------------------------------------------------

sugerirConexoes(Utilizador, Nivel, Sugestoes):-
    % Preenche Rede com a rede do utilizador ate ao nivel pretendido
    tamanhoRedeUtilizador(Utilizador, Nivel, Rede, _),

	% Substitui todos os sinonimos
    no(Utilizador, _, TagsUtilizadorRaw),
	normalizarSinonimos(TagsUtilizadorRaw, TagsUtilizador),

    % Remove todos os utilizadores da rede que nao tenham pelo menos 1
    % tag em comum com o Utilizador

    redeEmComum(TagsUtilizador, Rede, UtilizadoresSugeridos),
	write('Poss�veis Utilizadores Sugeridos: '), write(UtilizadoresSugeridos),
	nl,
    % Tenta encontrar caminho
    findall([X,Caminho],(
                member(X, UtilizadoresSugeridos),
                X \== Utilizador,

				% Devolve a lista de tags em comum entre X e o Utilizador.
                tagsEmComum(X, TagsUtilizador, TagsComuns),
				% Tenta encontrar um caminho do Utilizador até X de forma a que todos os
				% utilizadores do Caminho tenham, no mínimo, uma tag em comum
                dfsTagsComum(Utilizador, X, TagsComuns, Caminho),

				write('Utilizador '), write(X), write(': '), write(Caminho),
				nl
            ), Sugestoes),
	nl
	% imprimirLista(Sugestoes)
    .

redeEmComum(Tags, Rede, NovaRede):-
    findall(UserX, (
                member(UserX, Rede),
                no(UserX, _, TagsRawX),
				normalizarSinonimos(TagsRawX, TagsX),
                validaTagsComum(Tags, TagsX)
            ), NovaRede).

% Verifica se a lista no primeiro argumento tem pelo menos 1 elemento
% em comum com a lista no segundo argumento
validaTagsComum([], _):-
    fail.

validaTagsComum([Tag|_], ListaTags2):-
    member(Tag, ListaTags2),
    !.

validaTagsComum([_|ListaTags], ListaTags2):-
    validaTagsComum(ListaTags, ListaTags2).

% Devolve Lista de Tags em Comum entre os 2 Utilizadores
tagsEmComum(UtilizadorA, TagsUtilizador, ListaTags):-
    no(UtilizadorA, _, TagsRawA),
	normalizarSinonimos(TagsRawA, TagsA),
    intersecao(TagsA, TagsUtilizador, ListaTags).

% Verifica se no caminho existe pelo menos 1 tag da lista de tags em
% comum entre todos os utilizadores.
validaCaminhoTags(Caminho, ListaTags):-
    validaCaminhoTags2(Caminho, Caminho, ListaTags).

validaCaminhoTags2(_, _, []):-
    !,
    fail.

validaCaminhoTags2([], _, _):-
    !,
	true.

validaCaminhoTags2([Utilizador|Caminho], CaminhoOriginal, [Tag|ListaTags]):-
    no(Utilizador, _, TagsRawUtilizador),
	normalizarSinonimos(TagsRawUtilizador, TagsUtilizador),
    member(Tag, TagsUtilizador),
    !,
    validaCaminhoTags2(Caminho, CaminhoOriginal, [Tag|ListaTags]).

validaCaminhoTags2(_, CaminhoOriginal, [_|ListaTags]):-
    validaCaminhoTags2(CaminhoOriginal, CaminhoOriginal, ListaTags).


% --------------------------------------------------------------
% UC37 - Caminho Mais Forte
% --------------------------------------------------------------

% Facto Dinâmico - melhorSolucaoMaisForte(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMaisForte/2.

% Caminho Mais Forte - Sentido da Travessia (Unidirecional)
caminhoMaisForteUnidirecional(Origem, Destino, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisForteUnidirecional(Origem,Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisForte(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisForteUnidirecional(Origem, Destino):-
		% Define o melhor caminho com força igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMaisForte(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsUnidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMaisForteUnidirecional(Caminho),

		fail.

atualizaMelhorCaminhoMaisForteUnidirecional(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMaisForte(_, ForcaAtual),
		calculaForcaLigacaoUnidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMaisForte(_,_)),
		asserta(melhorSolucaoMaisForte(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, não será possível calcular mais forças
calculaForcaLigacaoUnidirecional([_], 0):-
	!.

calculaForcaLigacaoUnidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaForcaLigacaoUnidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, F1, _),
	Resultado is Forca + F1.


% Caminho Mais Forte - Dois Sentidos (Bidirecional)
caminhoMaisForteBidirecional(Origem, Destino, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisForteBidirecional(Origem,Destino);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisForte(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisForteBidirecional(Origem, Destino):-
		% Define o melhor caminho com força igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMaisForte(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsBidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMaisForteBidirecional(Caminho),

		fail.

atualizaMelhorCaminhoMaisForteBidirecional(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMaisForte(_, ForcaAtual),
		calculaForcaLigacaoBidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMaisForte(_,_)),
		asserta(melhorSolucaoMaisForte(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, não será possível
% calcular mais forças
calculaForcaLigacaoBidirecional([_], 0):-
	!.

calculaForcaLigacaoBidirecional([UtilizadorA,UtilizadorB|Caminho], Resultado):-
	calculaForcaLigacaoBidirecional([UtilizadorB|Caminho], Forca),
	ligacao(UtilizadorA, UtilizadorB, F1, F2),
	Resultado is Forca + F1 + F2.


% --------------------------------------------------------------
% UC38 - Caminho Mais Curto
% --------------------------------------------------------------

% Facto Dinamico - melhorSolucaoMinimoLigacoes(Caminho, LengthCaminho)
:- dynamic melhorSolucaoMinimoLigacoes/2.

% Caminho - Minimo Ligacoes Unidirecional
caminhoMinimoLigacoesUnidirecional(Origem, Destino, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMinimoLigacoesUnidirecional(Origem,Destino);true),
		!,

		% Retiramos o Melhor Caminho para ser retornado
		retract(melhorSolucaoMinimoLigacoes(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMinimoLigacoesUnidirecional(Origem, Destino):-
		% Define o melhor caminho com length de 10000 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMinimoLigacoes(_, 10000)),
		asserta(contadorSolucoes(0)),

		dfsMinimoLigacoesUnidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMinimoLigacoesUnidirecional(Caminho),

		fail.

atualizaMelhorCaminhoMinimoLigacoesUnidirecional(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara o Tamanho do Caminho atualmente registado como sendo o melhor
		% com o Tamanho do Caminho Recebido
		melhorSolucaoMinimoLigacoes(_, LengthAtual),
		length(Caminho, LengthCaminho),
		LengthCaminho < LengthAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMinimoLigacoes(_,_)),
		asserta(melhorSolucaoMinimoLigacoes(Caminho, LengthCaminho)).

% Caminho - Minimo Ligacoes Bidirecional
caminhoMinimoLigacoesBidirecional(Origem, Destino, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMinimoLigacoesBidirecional(Origem,Destino);true),
		!,

		% Retiramos o Melhor Caminho para ser retornado
		retract(melhorSolucaoMinimoLigacoes(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMinimoLigacoesBidirecional(Origem, Destino):-
		% Define o melhor caminho com length de 10000 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMinimoLigacoes(_, 10000)),
		asserta(contadorSolucoes(0)),

		dfsMinimoLigacoesBidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMinimoLigacoesBidirecional(Caminho),

		fail.

atualizaMelhorCaminhoMinimoLigacoesBidirecional(Caminho):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Compara o Tamanho do Caminho atualmente registado como sendo o melhor
		% com o Tamanho do Caminho Recebido
		melhorSolucaoMinimoLigacoes(_, LengthAtual),
		length(Caminho, LengthCaminho),
		LengthCaminho < LengthAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMinimoLigacoes(_,_)),
		asserta(melhorSolucaoMinimoLigacoes(Caminho, LengthCaminho)).


% --------------------------------------------------------------
% UC39 - Caminho Mais Seguro
% --------------------------------------------------------------

% Facto Dinâmico - melhorSolucaoMaisSeguro(Caminho, ForcaCaminho)
:- dynamic melhorSolucaoMaisSeguro/2.

% Caminho Mais Seguro - Sentido da Travessia (Unidirecional)
caminhoMaisSeguroUnidirecional(Origem, Destino, Limite, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisSeguroUnidirecional(Origem,Destino,Limite);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisSeguro(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisSeguroUnidirecional(Origem, Destino, Limite):-
		% Define o melhor caminho com for�a igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMaisSeguro(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsUnidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMaisSeguroUnidirecional(Caminho, Limite),

		fail.

atualizaMelhorCaminhoMaisSeguroUnidirecional(Caminho, Limite):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Valida se o caminho não contém forças abaixo do limite
		validaCaminhoMaisSeguroUnidirecional(Caminho, Limite),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMaisSeguro(_, ForcaAtual),
		calculaForcaLigacaoUnidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMaisSeguro(_,_)),
		asserta(melhorSolucaoMaisSeguro(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, não será possível calcular mais forças
validaCaminhoMaisSeguroUnidirecional([_], _):-
    !.

validaCaminhoMaisSeguroUnidirecional([UtilizadorA,UtilizadorB|Caminho], Limite):-
    ligacao(UtilizadorA, UtilizadorB, F1, _),
    F1 >= Limite,
    validaCaminhoMaisSeguroUnidirecional([UtilizadorB|Caminho], Limite).


% Caminho Mais Seguro - Dois Sentidos (Bidirecional)
caminhoMaisSeguroBidirecional(Origem, Destino, Limite, Caminho):-
		get_time(TempoInicio),

		(melhorCaminhoMaisSeguroBidirecional(Origem,Destino,Limite);true),
		!,

		% Retiramos o Caminho Mais Forte para ser retornado
		retract(melhorSolucaoMaisSeguro(Caminho,_)),

		% Retiramos o Numero de Solucoes para ser retornado
		retract(contadorSolucoes(NumeroSolucoes)),

		get_time(TempoFim),
		TempoExecucao is TempoFim - TempoInicio,

		write('Tempo de geracao da Solucao: '),
		write(TempoExecucao), write(' segundos'),nl,
		write('Numero de Solucoes: '), write(NumeroSolucoes), nl.

melhorCaminhoMaisSeguroBidirecional(Origem, Destino, Limite):-
		% Define o melhor caminho com força igual a 0 para garantir que será
		% substituido na primeira iteração
		asserta(melhorSolucaoMaisSeguro(_, 0)),
		asserta(contadorSolucoes(0)),

		dfsBidirecional(Origem, Destino, Caminho),
		atualizaMelhorCaminhoMaisSeguroBidirecional(Caminho, Limite),

		fail.

atualizaMelhorCaminhoMaisSeguroBidirecional(Caminho, Limite):-
		% Atualiza Contador de Solucoes
		retract(contadorSolucoes(NumeroSolucoes)),
		NovoNumeroSolucoes is NumeroSolucoes + 1,
		asserta(contadorSolucoes(NovoNumeroSolucoes)),

		% Valida se o caminho não contém forças abaixo do limite
		validaCaminhoMaisSeguroBidirecional(Caminho, Limite),

		% Compara a Força do Caminho atualmente registado como sendo o melhor
		% com a Força do Caminho Recebido
		melhorSolucaoMaisSeguro(_, ForcaAtual),
		calculaForcaLigacaoBidirecional(Caminho, ForcaCaminho),
		ForcaCaminho > ForcaAtual,

		% No caso de ser encontrado um caminho melhor (mais curto), é realizada a substituição
		retract(melhorSolucaoMaisSeguro(_,_)),
		asserta(melhorSolucaoMaisSeguro(Caminho, ForcaCaminho)).

% Quando apenas existir um elemento no caminho, não será possível calcular mais forças
validaCaminhoMaisSeguroBidirecional([_], _):-
    !.

validaCaminhoMaisSeguroBidirecional([UtilizadorA,UtilizadorB|Caminho], Limite):-
    ligacao(UtilizadorA, UtilizadorB, F1, F2),
    F1 >= Limite,
    F2 >= Limite,
    validaCaminhoMaisSeguroBidirecional([UtilizadorB|Caminho], Limite).
