% 2.
% ?- fronteira(espanha, franca) ; fronteira(franca, espanha).
% true.
%
% 3.
vizinho(P1, P2) :-
    fronteira(P1, P2) ; fronteira(P2, P1).

contSemPaises(C) :-
    continente(C),
    \+pais(_, C, _).

semVizinhos(L) :-
    pais(L, _, _),
    \+vizinho(L,_).

chegoLaFacil(P1, P2) :-
    vizinho(P1, P2).

chegoLaFacil(P1, P2) :-
    vizinho(P1, Pi),
    vizinho(P2, Pi),
    P1\==P2.

%4
%a)
potencia(_,0,1):-
    !.
potencia(B,N,P):-
    N > 0,
    N1 is N-1,
    potencia(B, N1, P1),
    P is P1 * B.
potencia(B,N,P):-
    N < 0,
    N1 is N+1,
    potencia(B, N1, P1),
    P is P1/B.

%b)
fatorial(0,1):-
    !.
fatorial(N,R):-
    N1 is N-1,
    fatorial(N1,R1),
    R is N*R1.

%c)

somatorio(J,K,R):-
    K < J,
    !,
    R = 0.

somatorio(J,K,_):-
    J == K,
    !.

somatorio(J,K,R):-
    J1 = J + 1,
    somatorio(J1, K, R1),
    R is J + R1.

%d)

divisao(A,B,Q):-
    A < B,
    Q is 0,
    !.

divisao(A,B,Q):-
    A1 = A - B,
    divisao(A1, B, Q1),
    Q is Q1 + 1.

resto(A,B,R):-
    A < B,
    R is A,
    !.

resto(A,B,R):-
    A1 = A - B,
    resto(A1, B, R).






