p([_],0).
p([x(A,B),x(C,D)|L],M):- A=<B, B=<C, C=<D, M1 is C-B,
p([x(C,D)|L],M2), ((M1<M2,M is M2);M is M1).


absoluto([],[],[]):-
    !.

absoluto([A|L1],[B|L2],[C|LF]):-
    (
        (   A > B, !, C is A - B);
        (   !, C is B - A)
    ),
    absoluto(L1,L2,LF).
