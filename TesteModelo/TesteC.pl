remove_iguais_seguidos([],[]):-!.

remove_iguais_seguidos([X|L],[X|LF]):-
    remove_iguais_seguidos2(X, L, LF).

remove_iguais_seguidos2(_,[],[]).

remove_iguais_seguidos2(X,[X|L],LF):-
    !,
    remove_iguais_seguidos2(X,L,LF).

remove_iguais_seguidos2(_,[Y|L],[Y|LF]):-
    !,
    remove_iguais_seguidos2(Y,L,LF).
