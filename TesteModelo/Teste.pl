mede_tensao(_,_,[],[],[],[]):-!.

mede_tensao(A,B,[X|LA],[Y|LB],[X|NA],[Y|NB]):-
    X>A, Y<B, !,
    mede_tensao(A,B,LA,LB,NA,NB).

mede_tensao(A,B,[X|LA],[_|LB],[X|NA],NB):-
    X>A, !,
    mede_tensao(A,B,LA,LB,NA,NB).

mede_tensao(A,B,[_|LA],[Y|LB],NA,[Y|NB]):-
    Y<B, !,
    mede_tensao(A,B,LA,LB,NA,NB).

mede_tensao(A,B,[_|LA],[_|LB],NA,NB):-
    mede_tensao(A,B,LA,LB,NA,NB).
