%PL3
%
%a) Grafo

liga(a,c).
liga(c,g).

liga(a,b).
liga(a,c).
liga(a,d).

liga(b,e).
liga(b,f).

liga(c,f).
liga(c,g).

liga(d,a).
liga(d,g).
liga(d,h).
liga(d,i).

liga(e,j).

liga(f,a).
liga(f,j).
liga(f,k).

liga(g,f).
liga(g,h).
liga(g,o).

liga(h,d).
liga(h,l).

liga(i,l).

liga(j,m).
liga(j,n).

liga(k,n).
liga(k,p).

liga(l,p).

% Testar Ligações
connect(X,Y):-
    liga(X,Y).
%    ;liga(Y,X).   -- Se o grafo for bidirecional

%b) Primeiro em Profundidade

dfs(Origem, Destino, Caminho):-
    dfs2(Origem, Destino, [Origem], Caminho).

% Condição Final: Nó Atual = Destino
dfs2(Destino, Destino, ListaAux, Caminho):-
    % A Lista Auxiliar encontra-se invertida porque os nós são adicionados na cabeça
    reverse(ListaAux, Caminho).

dfs2(NoAtual, Destino, ListaAux, Caminho):-
    % Testar Ligação entre NoAtual e qualquer X
    connect(NoAtual, X),

    % Testar se X já pertence à Lista para não visitar nós já visitados
    \+member(X, ListaAux),

    % Chamada Recursiva
    dfs2(X, Destino, [X|ListaAux], Caminho).

% dfs(a,j,Caminho).  Caminho = [a, b, e, j]

% c)
% dfs(a,j,Caminho).  Caminho = [a, c, g, f, j]

% d) Todos os Caminhos

% Para mostrar o conteúdo completo de uma lista
% set_prolog_flag(answer_write_options,[max_depth(0)]).

all_dfs(Origem, Destino, ListaCaminhos):-
    findall(Caminho, dfs(Origem, Destino, Caminho), ListaCaminhos).

% e) Melhor Caminho - Passa por menos nós

best_dfs(Origem, Destino, Caminho):-
    all_dfs(Origem, Destino, ListaCaminhos),
    melhor_caminho(ListaCaminhos, Caminho, _).

melhor_caminho([Caminho], Caminho, NumNos):-
    !,
    length(Caminho, NumNos).

melhor_caminho([Caminho|ListaCaminhos], MenorCaminhoAtual, MenorNumNosAtual):-
    melhor_caminho(ListaCaminhos, MenorCaminhoAtual1, MenorNumNosAtual1),
    length(Caminho, NumNosCaminho),
    (
        (
            NumNosCaminho < MenorNumNosAtual1, !, MenorCaminhoAtual = Caminho, MenorNumNosAtual is NumNosCaminho
        )
        ;
        (
            MenorCaminhoAtual = MenorCaminhoAtual1, MenorNumNosAtual is MenorNumNosAtual1
        )
    ).

% g) Primeiro em Largura

bfs(Origem, Destino, Caminho):-
    bfs2(Destino, [[Origem]], Caminho).

% Condição Final: Destino = Mó à cabeça do caminho atual
bfs2(Destino, [[Destino|T]|_], Caminho):-
    % Caminho Atual está invertido
    reverse([Destino|T], Caminho).

bfs2(Destino, [ListaAtual|Outros], Caminho):-
    ListaAtual = [Atual|_],
    % Calcular nós adjacentes não visitados e gerar um caminho novo com cada nó e caminho atual
    findall([X|ListaAtual],
            (Destino\==Atual,
            connect(Atual, X),
            \+ member(X, ListaAtual)),
            Novos),
    % Novos caminhos são colocados no final da lista para posterior exploração
    append(Outros, Novos, Todos),
    % Chamada Recursiva
    bfs2(Destino, Todos, Caminho).


bfs(Orig,Dest,Cam):-bfs2(Dest,[[Orig]],Cam).

%condicao final: destino = n� � cabe�a do caminho actual
bfs2(Dest,[[Dest|T]|_],Cam):-
	%caminho actual est� invertido
	reverse([Dest|T],Cam).

bfs2(Dest,[LA|Outros],Cam):-
	LA=[Act|_],
	%calcular todos os n�s adjacentes nao visitados e
	%gerar um caminho novo c/ cada n� e caminho actual
	findall([X|LA],
		(Dest\==Act,liga(Act,X),\+ member(X,LA)),
		Novos),
	%novos caminhos s�o colocados no final da lista
	%p/ posterior exploracao
	append(Outros,Novos,Todos),
	%chamada recursiva
	bfs2(Dest,Todos,Cam).
