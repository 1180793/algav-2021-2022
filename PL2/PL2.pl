%PL2
%1)
%
%a)

media([],0):-
    !.

media(L, M):-
    contaSoma(L, C, S),
    M is S / C.

contaSoma([],0,0):-
    !.

contaSoma([X|L],C,S):-
    contaSoma(L, C1, S1),
    C is C1 + 1,
    S is S1 + X.

%b)

menor([X],X):-
    !.

menor([X|L],M):-
    menor(L,M1),
    (
        (X < M1, !, M is X) ;
        M is M1
    ).

%c)

contarParImpar([],0,0):-
    !.

contarParImpar([X|L],P,I):-
    contarParImpar(L, P1, I1),
    R is X mod 2,
    (
        (R == 0, !, P is P1 + 1, I is I1) ;
        (I is I1 + 1, P is P1)
    ).

%d)

repetidos([X|L]):-
    member(X,L),
    !.

repetidos([_|L]):-
    repetidos(L).

%e)

menorFrenteLista([],[]):-
    !.

menorFrenteLista(L,[M|L]):-
    menor(L,M).

%f)

concatena([],L,L):-
    !.

concatena([X|L1],L2,[X|L]):-
    concatena(L1,L2,L).

%g)

linearizar([],[]):-
    !.

linearizar([[H|T]|L], LF):-
    !,
    append([H|T],L,L1), %append(Lista, Lista, Resultado)
    linearizar(L1,LF).

linearizar([X|L],[X|LF]):-
    linearizar(L,LF).

%h)

apaga1(_,[],[]):-
    !.

apaga1(X,[X|L],L):-
    !.

apaga1(X,[Y|L],[Y|L1]):-
    apaga1(X, L, L1).

%i)

apagaTodos(_,[],[]):-
    !.

apagaTodos(X,[X|L],L1):-
    !, %Para terminar ap�s a solu��o
    apagaTodos(X, L, L1).

apagaTodos(X,[Y|L],[Y|L1]):-
    apagaTodos(X, L, L1).

%j)

substituir(_,_,[],[]):-
    !.

substituir(X,Y,[X|L],[Y|L1]):-
    !,
    substituir(X,Y,L,L1).

substituir(X,Y,[Z|L],[Z|L1]):-
    !,
    substituir(X, Y,  L, L1).

%k)

inserir(X,1,L,[X|L]):-
    !.

inserir(X,N,[Y|L],[Y|L1]):-
    N1 is N-1,
    inserir(X,N1,L,L1).

%l)

inverter(L,LInvert):-
    inverte1(L,[],LInvert).

inverte1([],L,L):-
    !.

inverte1([H|L],Laux,LInvert):-
    inverte1(L,[H|Laux],LInvert).

%m)

uniao([],L,L):-
    !.

uniao([X|L1],L2,LUniao):-
    member(X,L2), %N�o adiciona elementos repetidos se X tamb�m pertencer a L2
    !,
    uniao(L1,L2,LUniao).

uniao([X|L1],L2,[X|LUniao]):-
    uniao(L1,L2,LUniao).

%n)

intersecao([],_,[]):-
    !.

intersecao([X|L1],L2,[X|LIntersecao]):-
    member(X,L2),
    !,
    intersecao(L1,L2,LIntersecao).

intersecao([_|L1],L2,LIntersecao):-
    intersecao(L1,L2,LIntersecao).

%o)

diferenca(L1,L2,LDiferenca):-
    diferenca1(L1,L2,LDif1),
    diferenca1(L2,L1,LDif2),
    append(LDif1,LDif2,LDiferenca).

diferenca1([],_,[]):-
    !.

diferenca1([X|L1],L2,LDiferenca):-
    member(X,L2),
    !,
    diferenca1(L1,L2,LDiferenca).

diferenca1([X|L1],L2,[X|LDiferenca]):-
    diferenca1(L1,L2,LDiferenca).



